const path = require("path");
const HDWalletProvider = require("@truffle/hdwallet-provider");

module.exports = {
    contracts_build_directory: path.join(__dirname, "../client/src/contracts/abis"),
    networks: {
        development: {
            host: "127.0.0.1",
            port: 8548,
            network_id: "*",
            gasLimit: 8000000,
        },

        rinkeby: {
            provider: () => {
                const mnemonic = "fantasy finger mask nominee fetch wine rule notable goddess result lake traffic"
                const project_id = '55ddbe7224334810b21809de770875de'
                return new HDWalletProvider(
                    mnemonic,
                    `https://rinkeby.infura.io/v3/${project_id}`
                );
            },
            network_id: 4
        }
    },
    compilers: {
        solc: {
            version: "0.8.3",
            settings: {
                optimizer: {
                    enabled: true,
                    runs: 200
                }
            }
        }
    }
};
