// SPDX-License-Identifier: MIT
pragma solidity >0.4.23 <0.9.0;

import "./Collection.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

contract Manufacturer is Ownable {

    bool public closed;
    address private creatorAddress;
    string public name;
    string public symbol;
    mapping(uint256 => address) collectionById;
    mapping(string => address) public collectionBySymbol;
    Counters.Counter public nrCollections;

    modifier onlyCreator {
        require(msg.sender == creatorAddress);
        _;
    }

    modifier isOpen {
        require(closed == false);
        _;
    }

    modifier isClosed {
        require(closed == true);
        _;
    }

    event NewCollection(
        address indexed contractAddress,
        string name,
        string symbol
    );

    constructor(string memory _name, string memory _symbol) {
        name = _name;
        symbol = _symbol;
        creatorAddress = msg.sender;
    }

    function close() onlyCreator isOpen external {
        closed = true;
    }

    function open() onlyCreator isClosed external {
        closed = false;
    }

    function getCollection(uint256 _id) public view returns (address) {
        require(_id < nrCollections._value, 'Collection doesn\'t exist');
        return collectionById[_id];
    }

    function newCollection(string memory _name, string memory _symbol, string memory _baseUri) onlyOwner isOpen external returns (address) {
        require(collectionBySymbol[_symbol] == address(0), 'Symbol already exists');
        require(bytes(_symbol).length == 3, 'Symbol must be of length 3');

        uint256 currentValue = nrCollections._value;
        address collectionAddress = address(new Collection(_name, _symbol, _baseUri));
        Counters.increment(nrCollections);

        collectionById[currentValue] = collectionAddress;
        collectionBySymbol[_symbol] = collectionAddress;
        emit NewCollection(collectionAddress, _name, _symbol);

        return collectionAddress;
    }

    function mintAssets(address _collectionAddress, uint256 _nrAssetsToMint, string memory _newBaseUri) onlyOwner isOpen external {
        require(Ownable(_collectionAddress).owner() == address(this), 'Collection not owned by this contract');

        Collection(_collectionAddress).mint(_nrAssetsToMint);
        Collection(_collectionAddress).setBaseUri(_newBaseUri);
    }

    function transferAsset(address collection, address to, uint256 id) onlyOwner isOpen external {
        ERC721(collection).transferFrom(address(this), to, id);
    }
}