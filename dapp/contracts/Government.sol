// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./Manufacturer.sol";
import "./Collection.sol";

contract Government is Ownable {

    mapping(address => address) public manufacturerByAddress;
    mapping(string => address) public manufacturerBySymbol;

    event AssignedManufacturer(
        address indexed who,
        address indexed contractAddress,
        string name,
        string symbol
    );

    function isManufacturer(address _address) external view returns (bool) {
        return manufacturerByAddress[_address] != address(0);
    }

    function getManufacturerContract(address _ofOwner) external view returns (address) {
        return manufacturerByAddress[_ofOwner];
    }

    function closeManufacturer(address _who) onlyOwner external {
        Manufacturer(_who).close();
    }

    function openManufacturer(address _who) onlyOwner external {
        Manufacturer(_who).open();
    }

    function newManufacturer(address _who, string memory _name, string memory _symbol) onlyOwner external {
        require(manufacturerByAddress[_who] == address(0), 'Owner mustn\'t be a non-manufacturer address');
        require(manufacturerBySymbol[_symbol] == address(0), 'Symbol must be unique');
        require(bytes(_symbol).length == 3, 'Symbol must be of length 3');

        address manufacturer = address(new Manufacturer(_name, _symbol));
        Ownable(manufacturer).transferOwnership(_who);

        manufacturerByAddress[_who] = manufacturer;
        manufacturerBySymbol[_symbol] = manufacturer;
        emit AssignedManufacturer(_who, manufacturer, _name, _symbol);
    }
}
