// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "./Manufacturer.sol";

contract Collection is ERC721, Ownable {
    Counters.Counter private supplyCounter;
    string public baseUri;

    constructor(
        string memory _name,
        string memory _symbol,
        string memory _baseUri
    ) ERC721(_name, _symbol) {
        baseUri = _baseUri;
    }

    function setBaseUri(string memory _baseUri) onlyOwner external {
        require(Manufacturer(owner()).closed() == false, 'Manufacturer is closed');
        baseUri = _baseUri;
    }

    function mint(uint256 _nrAssetsToMint) onlyOwner external {
        require(Manufacturer(owner()).closed() == false, 'Manufacturer is closed');

        for (uint256 i = 0; i < _nrAssetsToMint; i++) {
            _mint(msg.sender, supplyCounter._value);
            Counters.increment(supplyCounter);
        }
    }

    function _baseURI() internal view virtual override returns (string memory) {
        return baseUri;
    }

    function totalSupply() external view returns(uint256) {
        return supplyCounter._value;
    }
}
