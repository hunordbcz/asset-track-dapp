\chapter{Testing and Validation}
\pagestyle{fancy}

\begin{comment}
{\noindent\color{blue}This chapter should take about 5\% of the paper.}

\section{Section 1}
\section{Section 2}
\end{comment}

Testing the system is one of the most important parts of a project, which helps in making sure that the requirements are met and no side effects are present in the system. In the testing and validation phases, we can test the system in multiple ways, such as manual testing, automated testing, and performance validation.
\par
In this chapter, the systems' main scenarios are checked, and various flows are put to test, including error-prone scenarios. This way, the safety and correctness of the system are validated. In each scenario, performance is also measured by taking into account the blockchain specifics, such as the size of a block, the average transaction sizes in the given scenarios, and the time it takes to mine a block. In the end, the overall throughput of the application is also calculated.
\par
The main scenarios are those which modify the state of the contracts, but also simple scenarios will be included, such as making sure that the modified data can be requested and the system displays the new value correctly.
\emptyline
The system also offers constant information to the user for all the transactions that are created and are being processed. Figure \ref{fig:transactionNotifications} shows these states and that each notification also has a description and the transaction hash, which can be used to look up additional data on the Ethereum explorer.

\begin{figure}[H]
    \centering
    \begin{subfigure}[]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/6/processingTransaction.png}
        \caption{Processing Transaction Notification}
        \label{fig:pendingTransaction}
    \end{subfigure}
    \hfill
    \begin{subfigure}[]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/6/failedTransaction.png}
        \caption{Failed Transaction Notification}
        \label{fig:failedTransaction}
    \end{subfigure}
    \emptyline
    \emptyline
    \begin{subfigure}[]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figs/6/successfulTransaction.png}
        \caption{Successful Transaction Notification}
        \label{fig:successfulTransaction}
    \end{subfigure}
    \caption{Transaction notifications}
    \label{fig:transactionNotifications}
\end{figure}

\subsubsection{Government}
The most important scene in this context is the one where the manufacturers are added and presented/displayed for everyone. In this scenario, we will use random values for the name, symbol, and Ethereum address. First, we try a happy scenario, with an arbitrary name and a non-existent symbol in the system, and a valid Ethereum address.
\par
The transaction \textit{(hash \href{https://rinkeby.etherscan.io/tx/0x4dd71cf86100f5586c791fa0fd74f35bd0d3f11600355a05f20beb7f876bca18}{0x4dd7..ca18})} initiated by the web application with the parameters: \textit{name $\rightarrow$ Test; symbol $\rightarrow$ TST; ownerAddress $\rightarrow$ 0xD292..156e} was successful and we can confirm it by looking at the list in the web application, that the new entry appeared \textit{(see figure \ref{figure:addedManufacturer})}. It is also possible to confirm this by looking at the transaction details through the Testnet explorer, by using the transaction hash for lookup.

\begin{figure}[H]
    \center
    \includegraphics[width=\linewidth]{figs/6/addedManufacturer.png}
    \caption{GUI representation of newly added manufacturer}
    \label{figure:addedManufacturer}
\end{figure}

Secondly, in case we enter an invalid symbol, which is either not of length three or it's already occupied by another manufacturer, the transaction is rejected, as the contract defines specific restrictions, thus we get the expected behavior.

\subsubsection{Manufacturer}
The manufacturers' main responsibility is to add collections onto the blockchain, which then will contain the assets. These collections will also be displayed on the manufacturer page. For testing purposes, we will manually create a new collection, with arbitrary values.
\par
Let's use the following values: \textit{name $\rightarrow$ Galaxy; symbol $\rightarrow$ SX6; description $\rightarrow$ Samsung Galaxy S6 Phone; properties $\rightarrow$ Color, Display, Bluetooth, Storage and Battery}. To confirm that everything went okay, we can check the GUI, and see that the new collection appeared in the list \textit{(see figure \ref{figure:addedCollection})}. The transaction can be also confirmed by checking the transaction details on the explorer by using the hash \href{https://rinkeby.etherscan.io/tx/0x36fb8b8a75cdc12d28afbdf0140b957f7dfd0e0f904e8e36fab918ffb5761a07}{0x36fb..1a07}.

\begin{figure}[H]
    \center
    \includegraphics[width=\linewidth]{figs/6/addedCollection.png}
    \caption{GUI representation of newly added collection}
    \label{figure:addedCollection}
\end{figure}

To check for validation, too long data is entered into the fields and the web application simply doesn't create the transaction until it's valid.

\subsubsection{Collection}
This context refers to the creation of the assets and their management. The scenario which will be tested is the minting process. The collection page presents only those assets which are in the ownership of the connected account or the manufacturers in case the connected account is the owner of the manufacturer contract.
\par
For testing purposes, we will use the previously created collection and mint 300 assets, with a real-life example for the properties. The transaction finished with success and we can confirm it by viewing a freshly created asset in the application, which shows all the details from the blockchain and also from the decentralized storage. We can also confirm that it worked, by viewing the transaction details on the explorer, where it also shows specific details of the NFTs, as the Collection contract implements the ERC721 standard. The explorer confirms that 300 assets exist and the current owner is the manufacturer contract \textit{(see figure \ref{figure:mintedAssets})}.

\begin{figure}[H]
    \center
    \includegraphics[width=5cm]{figs/6/mintedAssets.png}
    \caption{Explorer showing information from collection}
    \label{figure:mintedAssets}
\end{figure}


\subsubsection{Performance}
In this section, we try to estimate the throughput of the system for the actions which modify the state of the contract. The calculations were done on the Rinkeby test network and at the moment of writing, the \textbf{block size is 30,000,000 gas}, \textbf{gas price is 2.5 gwei}, and the \textbf{average block time 15.03 seconds}.
\par

\begin{table}[H]
    {
    \ttfamily
    \begin{tabular}{|l|l|l|l|l|}
    \hline
    \rowcolor[HTML]{EFEFEF} 
    Action                  & Avg ETH Used & Avg Gas Used & TXs/Block   & TXs/Second  \\ \hline
    Open/Close Manuf.       & 0.00012884   & 51535.99984  & 582.117357 & 38.7303631 \\
    Add Manufacturer        & 0.009789178  & 3915671.187  & 7.66152176 & 0.5097486  \\
    Add Collection          & 0.005862707  & 2345082.792  & 12.7927253 & 0.85114606 \\
    Mint Assets ($\sim$180) & 0.012797139  & 5118855.584  & 5.86068497 & 0.38993247 \\
    Mint Assets (1)         & 0.00031323   & 125291.9996  & 239.440667 & 15.9308495 \\ \hline
    \end{tabular}
    }
    \centering
    \caption{Throughput calculations}
    \label{table:throughput}
\end{table}

Table \ref{table:throughput} shows the calculation of throughput for several transactions from this application. The formulas used are the following:
\begin{itemize}
    \item $\textbf{Average gas used} = Average ETH used / Gas Price$
    \item $\textbf{Transactions per Block} = Block size / Average Gas Used$
    \item $\textbf{Transactions per Second} = Transactions Per Block * Average Block Time^{-1}$
\end{itemize}