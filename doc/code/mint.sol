function mint(uint256 _nrAssetsToMint) onlyOwner external {
    require(Manufacturer(owner()).closed() == false, 'Manufacturer is closed');

    for (uint256 i = 0; i < _nrAssetsToMint; i++) {
        _mint(msg.sender, supplyCounter._value);
        Counters.increment(supplyCounter);
    }
}