function newManufacturer(address _who, string memory _name, string memory _symbol) onlyOwner external {
    require(manufacturerByAddress[_who] == address(0));
    require(manufacturerBySymbol[_symbol] == address(0));

    address manufacturer = address(new Manufacturer(_name, _symbol));
    Ownable(manufacturer).transferOwnership(_who);

    manufacturerByAddress[_who] = manufacturer;
    manufacturerBySymbol[_symbol] = manufacturer;
    emit AssignedManufacturer(_who, manufacturer, _name, _symbol);
}