function newCollection(string memory _name, string memory _symbol, string memory _baseUri) onlyOwner isOpen external returns (address) {
    require(collectionBySymbol[_symbol] == address(0), 'Symbol already exists');

    uint256 currentValue = nrCollections._value;
    address collectionAddress = address(new Collection(_name, _symbol, _baseUri));
    Counters.increment(nrCollections);

    collectionById[currentValue] = collectionAddress;
    collectionBySymbol[_symbol] = collectionAddress;
    emit NewCollection(collectionAddress, _name, _symbol);

    return collectionAddress;
}

    function mintAssets(address _collectionAddress, uint256 _nrAssetsToMint, string memory _newBaseUri) onlyOwner isOpen external {
    require(Ownable(_collectionAddress).owner() == address(this), 'Collection not owned by this contract');

    Collection(_collectionAddress).mint(_nrAssetsToMint);
    Collection(_collectionAddress).setBaseUri(_newBaseUri);
}

function transferAsset(address collection, address to, uint256 id) onlyOwner isOpen external {
    ERC721(collection).transferFrom(address(this), to, id);
}