import React, {useEffect} from 'react';
import './App.css';
import Navbar from "./components/Navbar";
import {Outlet} from "react-router-dom";
import {Box, Container} from "@mui/material";
import {useDispatch} from "react-redux";
import {connectWeb3} from "./providers/web3Slice";
import {initData} from "./providers/accountSlice";
import {initStore} from "./contracts/providers/governmentSlice";

export const Layout = () => {
    const dispatch = useDispatch();
    const startup = async () => {
        await dispatch(connectWeb3())
        await dispatch(initStore())
        await dispatch(initData())
    }

    useEffect(() => {
        startup()
            .then(console.debug)
            .catch(console.error)
    });

    return (
        <Box
            sx={{
                minHeight: '100vh',
                // backgroundColor: 'foreground.main',
                color: 'text.primary'
            }}
        >
            <Navbar/>
            <Container
                sx={{
                    marginTop: '3rem'
                }}
            >
                <Outlet/>
            </Container>
        </Box>
    );
}
