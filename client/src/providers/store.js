import {configureStore} from '@reduxjs/toolkit';
import web3Reducer from './web3Slice';
import notifications from './notifierSlice';
import government from "../contracts/providers/governmentSlice";
import transactions from "../contracts/providers/transactionsSlice";
import account from "./accountSlice";

export const store = configureStore({
    reducer: {
        web3: web3Reducer,
        notifications: notifications,
        transactions: transactions,
        government: government,
        account: account
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false
        }),
});
