import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {errorMessage} from "./notifierSlice";
import {initData} from "./accountSlice";

const initialState = {
    ethereum: null,
    providerActive: false,
    accountConnected: false,
    chainId: null,
    networkId: null,
    account: null
};

//Will check if a Metamask is installed & a wallet is connected or not
export const connectWeb3 = createAsyncThunk(
    'web3Slice/connectWeb3',
    async (userData, {dispatch, fulfillWithValue, rejectWithValue}) => {
        const ethereum = window.ethereum;
        console.debug("Checking if ethereum exists")
        if (!ethereum) {
            dispatch(errorMessage("Metamask is not installed in your browser!"));
            rejectWithValue("Metamask is not installed in your browser!");
        }

        console.debug("Get accounts without request")

        let networkId = await ethereum.request({
            method: 'net_version',
        });
        let chainId = await ethereum.request({
            method: 'eth_chainId',
        });
        await dispatch(setChainInfo({chainId, networkId}));

        let accounts = await ethereum.request({
            method: 'eth_accounts',
        });
        if (accounts.length > 0) {
            await dispatch(changeAccount(accounts))
        }

        ethereum.on('accountsChanged', async accounts => {
            await dispatch(changeAccount(accounts))
            dispatch(initData())
        })

        ethereum.on('chainChanged', _chainId => {
            //Documentation recommends reloading the page, unless must be done otherwise
            window.location.reload()
        });
    }, {
        condition: (userData, {getState}) => {
            const {web3} = getState()
            if (web3.providerActive) {
                return false;
            }
        }
    });

//Will check if a wallet is connected or not. If not, then ask for permission
export const connectWeb3Eagerly = createAsyncThunk(
    'web3Slice/connectWeb3Eagerly',
    async (userData, {getState, dispatch, rejectWithValue}) => {
        const {providerActive} = getState()
        if (!providerActive) {
            await dispatch(connectWeb3());
        }

        const ethereum = window.ethereum;
        try {
            let accounts = await ethereum.request({
                method: 'eth_requestAccounts',
            });
            dispatch(changeAccount(accounts))
        } catch (e) {
            dispatch(errorMessage(e.message))
            return rejectWithValue(e)
        }
    }, {
        condition: (userData, {getState}) => {
            const {web3} = getState()
            if (web3.accountConnected) {
                return false;
            }
        }
    });

export const web3Slice = createSlice({
    name: 'web3',
    initialState,
    reducers: {
        setChainInfo: (state, action) => {
            state.chainId = action.payload.chainId;
            state.networkId = action.payload.networkId;
        },
        changeAccount: (state, action) => {
            console.debug("Account changed", action)
            if (action.payload.length === 0) {
                state.accountConnected = false;
                state.account = null;
            } else {
                state.accountConnected = true;
                state.account = action.payload[0];
            }
        }
    },

    extraReducers: (builder) => {
        builder
            .addCase(connectWeb3.rejected, (state, action) => {
                state.status = 'rejected';
                console.error(action)
            })
            .addCase(connectWeb3.fulfilled, (state) => {
                state.providerActive = true;
                console.debug("Web3 connected")
            })

            .addCase(connectWeb3Eagerly.fulfilled, (state, action) => {
                state.accountConnected = true;
                console.debug("Account connected eagerly")
            })
            .addCase(connectWeb3Eagerly.rejected, (state) => {
                state.accountConnected = false;
                console.debug("Couldn't connect account")
            })
    },
});

export const {changeAccount, setChainInfo} = web3Slice.actions;

export default web3Slice.reducer;