import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    array: [],
    key: 1
};

const enqueueSnackbarUtil = (state, action) => {
    state.array = [...state.array, {
        key: state.key++,
        message: action.message,
        options: action.options
    }]
}

export const notificationsSlice = createSlice({
    name: 'notifications',
    initialState,
    reducers: {
        successMessage: (state, action) => {
            enqueueSnackbarUtil(state, {
                message: action.payload,
                options: {
                    variant: 'success',
                },
            });
        },
        errorMessage: (state, action) => {
            enqueueSnackbarUtil(state, {
                message: action.payload,
                options: {
                    variant: 'error',
                },
            });
        },
        warnMessage: (state, action) => {
            enqueueSnackbarUtil(state, {
                message: action.payload,
                options: {
                    variant: 'warn',
                },
            });
        },
        infoMessage: (state, action) => {
            enqueueSnackbarUtil(state, {
                message: action.payload,
                options: {
                    variant: 'info',
                },
            });
        },
        enqueueSnackbar: (state, action) => {
            enqueueSnackbarUtil(state, action.payload);
        },
        closeSnackbar: (state, action) => {
            state.array = state.array.map(notification => (
                (action.payload === true || notification.key === action.payload)
                    ? {...notification, dismissed: true}
                    : {...notification}
            ))
        },
        removeSnackbar: (state, action) => {
            state.array = state.array.filter(
                notification => notification.key !== action.payload
            )
        }
    },
});

export const {
    successMessage,
    errorMessage,
    infoMessage,
    warnMessage,
    enqueueSnackbar,
    closeSnackbar,
    removeSnackbar
} = notificationsSlice.actions;

export default notificationsSlice.reducer;