import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {sameAddress} from "../utils/Web3Utils";

const initialState = {
    address: null,
    isGovernmentOwner: false,
    isManufacturer: false,
    manufacturerAddress: null
};

export const initData = createAsyncThunk(
    'account/initData',
    async (_, {getState, dispatch, fulfillWithValue, rejectWithValue}) => {
        const data = {...initialState};
        const {web3, government} = getState();
        const currentAccount = web3.account;
        data.address = currentAccount;

        const governmentOwner = (await government.contract.getOwner());
        if (governmentOwner.error) {
            return rejectWithValue({error: governmentOwner.error.message, data})
        }
        if (sameAddress(currentAccount, governmentOwner)) {
            data.isGovernmentOwner = true;
        }

        const isManufacturer = await government.contract.isManufacturer(currentAccount);
        if (isManufacturer.error) {
            return rejectWithValue({error: isManufacturer.error.message, data});
        }
        if (isManufacturer === true) {
            data.isManufacturer = true;
            const manufacturerContract = await government.contract.getManufacturerContract(currentAccount);
            if (manufacturerContract.error) {
                return rejectWithValue({error: manufacturerContract.error.message, data})
            }


            data.manufacturerAddress = manufacturerContract;
        }

        return fulfillWithValue(data);
    }, {
        condition: (userData, {getState}) => {
            const {web3} = getState()
            if (!web3.accountConnected) {
                return false;
            }
        }
    });

export const accountSlice = createSlice({
    name: 'account',
    initialState,
    reducers: {
        clearUserData: (state) => {
            state.isGovernmentOwner = initialState.isGovernmentOwner;
            state.isManufacturer = initialState.isManufacturer;
            state.manufacturerAddress = initialState.manufacturerAddress;
        }
    },

    extraReducers: (builder) => {
        builder
            .addCase(initData.rejected, (state, action) => {
                console.error(action)
                if (!action.meta.rejectedWithValue) {
                    return;
                }

                const userData = action.payload.data;
                state.isGovernmentOwner = userData.isGovernmentOwner;
                state.isManufacturer = userData.isManufacturer;
                state.manufacturerAddress = userData.manufacturerAddress;
                state.address = userData.address;
            })
            .addCase(initData.fulfilled, (state, action) => {
                console.debug(action)

                const userData = action.payload;
                state.isGovernmentOwner = userData.isGovernmentOwner;
                state.isManufacturer = userData.isManufacturer;
                state.manufacturerAddress = userData.manufacturerAddress;
                state.address = userData.address;
            })
    },
});

export const {clearUserData} = accountSlice.actions;

export default accountSlice.reducer;