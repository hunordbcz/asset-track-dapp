import {CID, create, IPFSHTTPClient} from "ipfs-http-client";
import {AddResult} from "ipfs-http-client/types/src/add-all";
import {GetResult} from "ipfs-core-types/types/src/dag";

const ipfs: IPFSHTTPClient = create({
    host: 'ipfs.infura.io',
    port: 5001,
    protocol: 'https',
    apiPath: 'api/v0',
    headers: {
        authorization: 'Basic ' + Buffer.from("28CnlLxytBbt0dyvjeTCEOctNZv:7b3fbf9fa6f9efcaf43849406b3d3006").toString('base64')
    }
})

const getVersion = async () => {
    return await ipfs.version();
}

const collectResults = async <T extends any>(toCollect: AsyncIterable<T>): Promise<Array<T>> => {
    let results = [];
    for await (const obj of toCollect) {
        results.push(obj)
    }

    return results;
};

const getPinnedFiles = async () => {
    return collectResults(ipfs.pin.ls());
}

const getByCID = async (cid: string, strict: boolean = false): Promise<Uint8Array> => {
    const uint8Arrays = await collectResults(
        ipfs.cat(formatCID(cid, strict))
    );

    let result = new Uint8Array();
    for (let i = 0; i < uint8Arrays.length; i++) {
        let tmp = new Uint8Array(result.byteLength + uint8Arrays[i].byteLength);
        tmp.set(new Uint8Array(result), 0);
        tmp.set(new Uint8Array(uint8Arrays[i]), result.byteLength);
        result = tmp;
    }

    return result;
}

const getByCIDasString = async (cid: string, strict: boolean = false): Promise<string> => {
    return Buffer.from(await(getByCID(cid, strict))).toString('utf8');
}

const getDag = (cid: string): Promise<GetResult> => {
    return ipfs.dag.get(CID.parse(formatCID(cid, true)));
}

const dagToJson = async (cid: string, shouldInclude: (value: any) => boolean = () => true): Promise<Array<any>> => {
    return new Promise((resolve, reject) => {
        getDag(cid).then(dagData => {
            console.debug('dagData', dagData)
            const links = dagData.value.Links;
            let promises = [];
            for (const link of links) {
                if (!shouldInclude(link.Name)) {
                    continue;
                }
                promises.push(cidToContent(link.Hash.toString(), link.Name, link.Name.includes('image')))
            }

            Promise.all(promises)
                .then(values => {
                    resolve(values)
                })
                .catch(reject)
        })
    })
}

const cidToContent = async (cid: string, path: string, isImage: boolean): Promise<any> => {
    console.debug('cidToContent', cid, path)
    return new Promise(async (resolve, reject) => {
        const content = await getByCID(cid);
        resolve({
            path,
            content: isImage ? new Blob([content.buffer], {type: 'image/png'}) : content
        })
    })
}

const addFiles = async (files: Array<File>,
                        onProgress: (progress: number) => any = () => {
                        }
): Promise<Array<AddResult>> => {
    const start = new Date();
    let addResults = ipfs.addAll(files,
        {
            progress: onProgress,
            wrapWithDirectory: true
        });

    const results = await collectResults(addResults);
    console.debug("Took " + (new Date().getTime() - start.getTime()) + "ms for adding "  + files.length + " files");

    return results;
}

const formatCID = (cid: string, strict: boolean): string => {
    if (cid.length > 46 && strict) {
        return cid.substr(0, 46);
    }
    return cid;
}

export const IPFS = {
    getVersion,
    getPinnedFiles,
    getByCID,
    getByCIDasString,
    getDag,
    dagToJson,
    addFiles
}