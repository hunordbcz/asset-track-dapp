export const eventToComponent = (event) => {
    switch (event.event) {
        case 'OwnershipTransferred':
            return {}
        case 'AssignedManufacturer':
            return {}
        default:
            return {}
    }
}