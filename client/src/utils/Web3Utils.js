import CollectionContract from "../contracts/collectionContract";
import ManufacturerContract from "../contracts/manufacturerContract";
import Web3 from "web3";

export const formatAddress = address => {
    return address &&
        `0x${address.slice(2, 6).toUpperCase()}...${address.slice(address.length - 4, address.length).toUpperCase()}`
}

export const sameAddress = (a, b) => {
    if (typeof a !== "string") {
        return false;
    }
    if (typeof b !== "string") {
        return false;
    }
    return a.toUpperCase() === b.toUpperCase();
}

export const validCollectionOverall = (governmentContract, collectionAddress, dispatch) => {
    return new Promise(async (resolve, reject) => {
        if (governmentContract === undefined || collectionAddress === undefined) {
            reject("Undefined parameters");
            return;
        }

        const collectionContract = new CollectionContract(collectionAddress, dispatch);
        const manufacturerContract = new ManufacturerContract(await collectionContract.getOwner(), dispatch);
        const manufacturerAddress = manufacturerContract.getAddress();

        governmentContract.validManufacturer(manufacturerAddress)
            .then(valid => {
                if (!valid) {
                    resolve(false)
                    return;
                }

                manufacturerContract.validCollection(collectionAddress)
                    .then(valid => {
                        resolve(valid)
                    })
            })
            .catch(() => {
                resolve(false)
            })
    })
}

export const getDateFromBlock = async (nrBlock, web3 = new Web3(window.ethereum)) => {
    const blockData = await web3.eth.getBlock(nrBlock);
    return new Date(blockData.timestamp * 1000);
}

export const isEmptyAddress = address => {
    return sameAddress(address, '0x0000000000000000000000000000000000000000');
}