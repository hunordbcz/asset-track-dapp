import {createTheme} from "@mui/material";

export const darkTheme = createTheme({
    palette: {
        mode: 'light',
        // primary: {
        //     main: '#e2e2e5',
        // },
        // secondary: {
        //     main: '#1a202c',
        // },
        // foreground: {
        //     main: '#2d3748'
        // },
        // background: {
        //     main: '#1a202c'
        // }
    },
});