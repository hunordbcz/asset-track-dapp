import React, {forwardRef, useCallback} from 'react';
import {SnackbarContent, useSnackbar} from 'notistack';
import {Alert, AlertTitle, CircularProgress, Tooltip, Typography} from "@mui/material";
import {useSelector} from "react-redux";
import CopyIcon from "@mui/icons-material/ContentCopyRounded";
import CheckIcon from "@mui/icons-material/Check";
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";

const SnackMessage = forwardRef((props, ref) => {

    const {closeSnackbar} = useSnackbar();
    const transaction = useSelector(state => state.transactions.array[props.id]);

    const handleDismiss = useCallback(() => {
        if (!transaction.isDone) {
            return;
        }
        closeSnackbar(props.id);
    }, [props.id, closeSnackbar]);

    return (
        <SnackbarContent ref={ref}>
            <Alert
                variant='outlined'
                iconMapping={{
                    info: <CircularProgress size='2rem'
                                            thickness={5} color='inherit'/>,
                    success: <CheckIcon sx={{fontSize: '2rem'}} color='inherit'/>,
                    error: <ErrorOutlineIcon sx={{fontSize: '2rem'}} color='inherit'/>,
                }}
                severity={
                    !transaction.isDone
                        ? 'info'
                        : transaction.isError
                            ? 'error'
                            : 'success'
                }
                onClose={handleDismiss}
                sx={{
                    // backgroundColor: '#1a202c',
                    minWidth: '275px'
                }}
            >
                <AlertTitle>
                    {!transaction.isDone
                        ? 'Processing Transaction'
                        : transaction.isError
                            ? 'Transaction Error'
                            : 'Transaction Successful'
                    }
                </AlertTitle>
                {transaction.description}

                <Typography variant="caption" display="block" mt='1rem' gutterBottom>
                    {props.id.substr(2, 12) + '...' + props.id.substr(props.id.length - 10)}
                    <Tooltip title='Copy Hash'>
                        <CopyIcon sx={{
                            marginLeft: '1rem',
                            fontSize: '1rem',
                            cursor: 'pointer'
                        }} onClick={() => navigator.clipboard.writeText(props.id)}/>
                    </Tooltip>
                </Typography>
            </Alert>
        </SnackbarContent>
    );
});

export default SnackMessage;