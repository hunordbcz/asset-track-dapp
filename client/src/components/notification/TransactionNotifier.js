import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useSnackbar} from 'notistack';
import {removeTxByHash} from "../../contracts/providers/transactionsSlice";

let displayed = [];
let willClose = {};

const useNotifier = () => {
    const dispatch = useDispatch();
    const transactions = useSelector(store => store.transactions.array || []);
    const {enqueueSnackbar, closeSnackbar} = useSnackbar();

    const storeDisplayed = (hash) => {
        displayed = [...displayed, hash];
    };

    const removeDisplayed = (hash) => {
        displayed = [...displayed.filter(key => hash !== key)];
        delete willClose[hash];
    };

    const closeWithTimeout = (hash) => {
        if (willClose[hash] !== undefined) return;
        console.debug('assigning timeout', hash)

        willClose[hash] = setTimeout(() => {
            closeSnackbar(hash);
        }, 10000);
    }

    React.useEffect(() => {
        for (const [hash, {isDone, isDismissed}] of Object.entries(transactions)) {
            if (isDismissed) {
                closeSnackbar(hash);
                continue;
            }

            if (isDone) {
                closeWithTimeout(hash)
                continue;
            }

            if (displayed.includes(hash)) {
                continue;
            }

            enqueueSnackbar(
                '', //Will take data by itself
                {
                    key: hash,
                    persist: true,
                    onExited: (event, hash) => {
                        dispatch(removeTxByHash(hash));
                        removeDisplayed(hash);
                    },
                });

            // keep track of snackbars that we've displayed
            storeDisplayed(hash);
        }
    }, [transactions, closeSnackbar, enqueueSnackbar, dispatch, willClose]);
};

export default useNotifier;
