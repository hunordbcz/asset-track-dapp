import React, {forwardRef, useCallback} from 'react';
import {SnackbarContent, useSnackbar} from 'notistack';
import {Box, Card, Grid, IconButton, LinearProgress, Tooltip, Typography} from "@mui/material";
import {Close as CloseIcon} from '@mui/icons-material';
import {useSelector} from "react-redux";
import CopyIcon from "@mui/icons-material/ContentCopyRounded";

const SnackMessage = forwardRef((props, ref) => {

    const {closeSnackbar} = useSnackbar();
    const transaction = useSelector(state => state.transactions.array[props.id]);

    const handleDismiss = useCallback(() => {
        closeSnackbar(props.id);
    }, [props.id, closeSnackbar]);

    return (
        <SnackbarContent ref={ref}>
            <Card
                sx={{
                    backgroundColor: '#1a202c',
                    minWidth: '300px'
                }}
            >
                <Box
                    paddingX='1rem'
                >
                    <Grid
                        marginTop='.75rem'
                        marginRight='.5rem'
                        container
                        justifyContent='space-between'
                        alignItems='center'
                    >
                        <Typography variant='h6'>
                            {!transaction.isDone
                                ? 'Processing Transaction'
                                : transaction.isError
                                    ? 'Transaction Error'
                                    : 'Transaction Successful'
                            }
                        </Typography>
                        <IconButton disabled={!transaction.isDone} onClick={handleDismiss}>
                            <CloseIcon/>
                        </IconButton>
                    </Grid>

                    <Typography variant='body1' gutterBottom sx={{fontWeight: 600}}>
                        {transaction.description}
                    </Typography>

                    <Typography variant="caption" display="block" mt='1rem' gutterBottom>
                        {props.id.substr(2, 12) + '...' + props.id.substr(props.id.length - 10)}
                        <Tooltip title='Copy Hash'>
                            <CopyIcon sx={{
                                marginLeft: '1rem',
                                fontSize: '1rem',
                                cursor: 'pointer'
                            }} onClick={() => navigator.clipboard.writeText(props.id)}/>
                        </Tooltip>
                    </Typography>
                </Box>

                <LinearProgress
                    color={!transaction.isDone ? 'info' :
                        transaction.isError ? 'error' : 'success'}
                    variant={transaction.isDone ? 'determinate' : 'indeterminate'}
                    value={100}
                    sx={{
                        height: '.4rem'
                    }}
                />
            </Card>
        </SnackbarContent>
    );
});

export default SnackMessage;