import {useMatch, useNavigate, useResolvedPath} from "react-router-dom";
import {Button} from "@mui/material";

export default function ActiveNavButton({children, path, onClick, ...props}) {
    const navigate = useNavigate();
    const resolved = useResolvedPath(path);
    const match = useMatch({path: resolved.pathname, end: true});


    return (
        <Button
            variant="contained"
            disableElevation
            sx={{
                fontWeight: 'bold',
                borderRadius: '.5rem',
                backgroundColor: !match ? '#cecece' : '',
                color: 'black',
            }}
            onClick={event => onClick ? onClick(event) : navigate(path)}
            {...props}
        >
            {children}
        </Button>
    );
}