import {Navigate, Outlet} from "react-router-dom";
import {useSelector} from "react-redux";

export const ProtectedRoute = ({
                                   requireGovernment = false,
                                   requireManufacturer = false,
                                   redirectPath = '/', children
                               }) => {
    const {isGovernmentOwner, isManufacturer} = useSelector(state => state.account);

    if (!isGovernmentOwner && requireGovernment) {
        return <Navigate to={redirectPath} replace/>
    }

    if (!isManufacturer && requireManufacturer) {
        return <Navigate to={redirectPath} replace/>
    }

    return children ? children : <Outlet/>;
};