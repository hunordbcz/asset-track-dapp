import {Avatar, Chip, Tooltip} from "@mui/material";
import Identicon from "./Identicon";
import {formatAddress} from "../utils/Web3Utils";
import {useDispatch} from "react-redux";
import {infoMessage} from "../providers/notifierSlice";

export default function ActionIdenticon({account, tooltipMessage, ...props}) {
    const dispatch = useDispatch();

    function handleClick() {
        navigator.clipboard.writeText(account)
        dispatch(infoMessage("Copied address to clipboard"))
    }

    return (
        <Tooltip title={tooltipMessage ? tooltipMessage : "Copy address to clipboard"}>
            <Chip
                onClick={handleClick}
                avatar={<Avatar><Identicon account={account}/></Avatar>}
                label={formatAddress(account)}
                variant='outlined'
                color='primary'
                {...props}
            />
        </Tooltip>
    )
}