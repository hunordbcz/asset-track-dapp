import {useEffect, useRef} from "react";
// @ts-ignore
import Jazzicon from "@metamask/jazzicon";
import styled from "@emotion/styled";

const StyledIdenticon = styled.div`
  //height: 1rem;
  //width: 1rem;
  //border-radius: 1.125rem;
  //background-color: black;
`;

type Props = {
    account: any;
};

export default function Identicon({account}: Props) {
    const ref = useRef<HTMLDivElement>();

    useEffect(() => {
        if (account && ref.current) {
            ref.current.innerHTML = "";
            ref.current.appendChild(Jazzicon(16, parseInt(account.slice(2, 10), 16)));
        }
    }, [account]);

    return <StyledIdenticon ref={ref as any}/>;
}
