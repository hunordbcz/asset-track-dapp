import React, {useEffect, useMemo, useState} from 'react';
import {useDropzone} from 'react-dropzone';
import {Button} from "@mui/material";
import HighlightOffIcon from '@mui/icons-material/HighlightOff';

const baseStyle = {
    display: 'flex',
    alignItems: 'center',
    height: '200px',
    borderStyle: 'dashed',
    borderRadius: '10px',
    backgroundColor: '#1a202c',
    color: '#bdbdbd',
};

const thumb = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    maxHeight: '100%',
    height: '100%',
};

const thumbInner = {
    position: 'relative',
    boxShadow: '0 0 10px black',
    overflow: 'hidden'
};

const img = {
    width: 'auto',
    maxHeight: '200px'
};


export default function DropzoneLocal({onFilesModified}) {
    const {
        getRootProps,
        getInputProps,
        isFocused,
        isDragAccept,
        isDragReject,
        fileRejections,
    } = useDropzone({
        accept: 'image/*',
        onDrop: acceptedFiles => internalFileModified(acceptedFiles),
        maxFiles: 1
    });

    const style = useMemo(() => ({
        ...baseStyle,
    }), [
        isFocused,
        isDragAccept,
        isDragReject
    ]);

    const [files, setFiles] = useState([]);

    useEffect(() => {
        files.forEach(file => URL.revokeObjectURL(file.blob));
    }, [files]);

    function clearImage() {
        setFiles([]);
        if (onFilesModified !== undefined) {
            onFilesModified([])
        }
    }

    function internalFileModified(acceptedFiles) {
        if (onFilesModified !== undefined) {
            onFilesModified(acceptedFiles)
        }

        let files = acceptedFiles.map(file => Object.assign(file, {
            blob: URL.createObjectURL(file)
        }));
        setFiles(files)
    }

    return (
        files.length === 0
            ? <div>
                <div {...getRootProps({style, className: 'dropzone'})}>
                    <input {...getInputProps()} />
                    <p>Drag 'n' drop an image here, or click to select image</p>
                </div>
                {fileRejections.length > 0 ? <small style={{
                    color: 'red'
                }}>{fileRejections[0].errors[0].message}</small> : ''}
            </div>
            : <div style={thumb} key={files[0].name}>
                <div style={thumbInner}>
                    <img
                        src={files[0].blob}
                        style={img}
                    />
                    <Button
                        variant={'outlined'}
                        color={'error'}
                        size={'small'}
                        onClick={clearImage}
                        sx={{
                            position: 'absolute',
                            right: 5,
                            top: 5
                        }}
                        startIcon={<HighlightOffIcon color={'error'}/>}>
                        Clear
                    </Button>
                </div>
            </div>

    );
}