import {
    Backdrop,
    Box,
    Button,
    Chip,
    CircularProgress,
    Divider,
    Grid,
    Stack,
    TextField,
    Typography
} from "@mui/material";
import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {errorMessage} from "../providers/notifierSlice";
import Dropzone from "./Dropzone";
import {IPFS} from "../utils/IPFSHandler";
import Compressor from 'compressorjs';

export default function NewCollection({contract, onSuccess}) {
    const dispatch = useDispatch();
    // const [contract] = useOutletContext();
    const account = useSelector(state => state.web3.account);

    const [name, setName] = useState('');
    const [symbol, setSymbol] = useState('');
    const [description, setDescription] = useState('');
    const [image, setImage] = useState(null);
    const [property, setProperty] = useState('');
    const [properties, setProperties] = useState([]);

    const [awaitingTx, setAwaitingTx] = useState(false);
    // const [isSaving, setIsSaving] = useState(false);

    const isValid = {
        name: () => name.length < 10 && name.length !== 0,
        symbol: () => symbol.length === 3,
        description: () => description.length < 30 && description.length !== 0,
        property: () => property.length >= 3,
        properties: () => properties.length !== 0
    }

    function addCollection() {
        let error = false;
        if (!isValid.name()) {
            dispatch(errorMessage('Invalid name'));
            error = true;
        }
        if (!isValid.symbol()) {
            dispatch(errorMessage('Invalid symbol'));
            error = true;
        }
        if (!isValid.description()) {
            dispatch(errorMessage('Invalid description'));
            error = true;
        }
        if (!isValid.properties()) {
            dispatch(errorMessage('There must be at least 1 property'));
            error = true;
        }
        if (image === null) {
            dispatch(errorMessage('Image cannot be empty'));
            error = true;
        }

        if (error) {
            return;
        }

        setAwaitingTx(true)
        // setIsSaving(true)
        new Promise(async (resolve, reject) => {
            let cids = await uploadDataToIpfs();
            console.debug('cids for uploaded files', cids);
            let folderCid = cids.find(obj => obj.path === '').cid.toString();

            uploadDataToBlockchain(folderCid)
                .then(result => {
                    resolve(result)
                })
                .catch(error => {
                    reject(error)
                })
        }).then(result => {
            if (typeof onSuccess === 'function') {
                onSuccess();
            }
        }).catch(error => {
            console.error(error)
            dispatch(errorMessage('Couldn\'t create collection'))
        }).finally(() => {
            setAwaitingTx(false)
        })
    }

    const uploadDataToIpfs = async () => {
        let allData = [
            { // image
                path: 'image.png',
                content: image
            },
            {
                path: 'collection.json',
                content: JSON.stringify({
                    name: name,
                    description: description,
                    manufacturer: contract.getAddress(),
                    attributes: properties
                })
            }
        ]
        console.debug('Adding files to IPFS: ', allData)

        return await IPFS.addFiles(allData);
    }

    const uploadDataToBlockchain = (collectionCID) => {
        return contract.newCollection(name, symbol, collectionCID, account);
    }

    const handleDelete = index => {
        setProperties(properties.filter((val, idx) => idx !== index))
    };

    const handleKeyPress = event => {
        if (event.key === 'Enter') {
            addProperty()
        }
    }

    const addProperty = () => {
        if (property === '') {
            return;
        }
        setProperties([...properties, property])
        setProperty('')
    }

    const imageModified = (images) => {
        if (images.length <= 0) {
            setImage(null)
            return;
        }

        new Compressor(images[0], {
            quality: 0.7,
            success(result) {
                setImage(result)
            },
            error(err) {
                console.error(err.message);
            },
        });
    }

    return (
        <Box>
            {/*<Typography variant='h4'>*/}
            {/*    Add new Collection*/}
            {/*</Typography>*/}
            <Stack
                paddingX={2}
                marginY={3}
                textAlign='center'
                spacing={2}
            >
                <Grid container item spacing={2}
                      alignItems="stretch">
                    <Grid item xs={4}>
                        <Dropzone onFilesModified={imageModified}/>
                    </Grid>
                    <Grid item xs={8}>
                        <Stack direction="column" spacing={2}>
                            <Stack direction="row" spacing={2}>
                                <TextField
                                    fullWidth
                                    value={name}
                                    onChange={event => setName(event.target.value)}
                                    error={!isValid.name() && name.length !== 0}
                                    label="Name"
                                    helperText="Long name consisting of 10 characters"
                                />
                                <TextField
                                    fullWidth
                                    value={symbol}
                                    onChange={event => setSymbol(event.target.value.toUpperCase())}
                                    error={!isValid.symbol() && symbol.length !== 0}
                                    variant='outlined'
                                    label="Symbol"
                                    helperText="Short name consisting of 3 characters"
                                />
                            </Stack>
                            <TextField
                                fullWidth
                                value={description}
                                onChange={event => setDescription(event.target.value)}
                                error={!isValid.description() && description.length !== 0}
                                label="Description"
                                helperText="Maximum 30 characters"
                            />
                        </Stack>
                    </Grid>
                </Grid>
                <Divider/>
                <Stack
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="baseline"
                    spacing={2}
                >
                    <TextField
                        value={property}
                        onChange={event => setProperty(event.target.value)}
                        onKeyPress={handleKeyPress}
                        error={!isValid.property() && property.length !== 0}
                        label="Property name"
                        helperText="Add possible properties"
                    />
                    <Button variant={'outlined'} onClick={addProperty}>
                        Add Property
                    </Button>
                </Stack>
                <Stack
                    direction="column"
                    justifyContent="flex-start"
                    alignItems="flex-start"
                >
                    <Typography variant={'caption'}>List of properties:</Typography>
                    <Box>
                        {properties.map(
                            (property, index) => <Chip sx={{
                                margin: '.1rem'
                            }} key={index} label={property} variant="outlined"
                                                       onDelete={() => handleDelete(index)}/>
                        )}
                    </Box>
                </Stack>
                <Button
                    variant={'contained'}
                    color={'info'}
                    onClick={() => addCollection()}
                >
                    Submit
                </Button>

            </Stack>
            <Backdrop
                sx={{color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1}}
                open={awaitingTx}
                // onClick={handleClose}
            >
                <CircularProgress color="inherit"/>
            </Backdrop>
        </Box>
    )
}