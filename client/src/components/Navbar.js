import {Box, Chip} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {connectWeb3Eagerly} from "../providers/web3Slice";
import {useNavigate} from "react-router-dom";
import LoadingButton from '@mui/lab/LoadingButton';
import {useState} from "react";
import ActionIdenticon from "./ActionIdenticon";
import HomeIcon from '@mui/icons-material/Home';

export default function Navbar() {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const account = useSelector(state => state.web3.account);
    const accountConnected = useSelector(state => state.web3.accountConnected);

    const [isLoading, setIsLoading] = useState(false);

    async function connectMetamask() {
        setIsLoading(true)
        dispatch(connectWeb3Eagerly())
            .unwrap()
            .catch(console.error)
            .finally(() => {
                setIsLoading(false)
            })
    }

    return (
        <Box
            display="grid"
            gridTemplateColumns="120px 1fr 120px"
            gap={2}
            padding='.75rem'
        >
            <Box
                justifySelf='flex-start'
            >
                <Chip
                    label="Home"
                    color={'primary'}
                    onClick={() => navigate('/', {replace: true})}
                    avatar={<HomeIcon/>}
                />
            </Box>
            <Box
                justifySelf='center'
                width='fit-content'
            >
            </Box>
            <Box
                width='max-content'
                justifySelf='flex-end'
            >
                {!accountConnected ?
                    <LoadingButton
                        loading={isLoading}
                        color='secondary'
                        variant='contained'
                        onClick={connectMetamask}
                    >
                        Connect MetaMask
                    </LoadingButton>
                    :
                    <ActionIdenticon account={account}/>
                }

            </Box>
        </Box>
    )
}