import Web3 from "web3";
import {connectWeb3} from "../providers/web3Slice";
import ManufacturerABI from "./abis/Manufacturer.json";
import {receiveTxAsPromise} from "./providers/transactionsSlice";

export default class ManufacturerContract {
    constructor(address, dispatch) {
        this.address = address;
        this.dispatch = dispatch;
    }

    async getWeb3ContractSafe() {
        if (this.web3Contract === undefined) {
            await this.dispatch(connectWeb3());

            const web3Instance = new Web3(window.ethereum);
            this.web3Contract = new web3Instance.eth.Contract(ManufacturerABI.abi, this.address);
        }

        return this.web3Contract;
    }

    getWeb3Contract() {
        if (this.web3Contract === undefined) {
            const web3Instance = new Web3(window.ethereum);
            this.web3Contract = new web3Instance.eth.Contract(ManufacturerABI.abi, this.address);
        }

        return this.web3Contract;
    }

    getAddress() {
        return this.address;
    }

    async getOwner() {
        return await (await this.getWeb3ContractSafe()).methods.owner().call();
    }

    async getName() {
        return await this.getWeb3Contract().methods.name().call();
    }

    async getSymbol() {
        return await this.getWeb3Contract().methods.symbol().call();
    }

    async getNrCollections() {
        return await this.getWeb3Contract().methods.nrCollections().call();
    }

    async isClosed() {
        return await this.getWeb3Contract().methods.closed().call();
    }

    async getCollectionContractBySymbol(symbol) {
        return await this.getWeb3Contract().methods.collectionBySymbol(symbol).call()
    }

    async newCollection(name, symbol, collectionCID, account) {
        if (!collectionCID.endsWith("/")) {
            collectionCID = collectionCID + "/";
        }
        const web3 = await this.getWeb3ContractSafe();
        return receiveTxAsPromise(this.dispatch,
            () => web3.methods.newCollection(name, symbol, collectionCID).send({
                from: account
            }),
            'Add new Collection'
        );
    }

    async validCollection(address) {
        if (!Web3.utils.isAddress(address)) {
            return new Promise((resolve, reject) => reject('Invalid address specified'))
        }

        return new Promise(async (resolve, reject) => {
            (await this.getWeb3Contract()).getPastEvents('NewCollection', {
                fromBlock: 'earliest',
                filter: {
                    contractAddress: address
                }
            })
                .then(events => {
                    resolve(events.length > 0)
                })
                .catch(() => {
                    resolve(false)
                })
        });
    }

    async mintAssets(address, count, tokenUri, account) {
        if (!tokenUri.endsWith("/")) {
            tokenUri = tokenUri + "/";
        }
        console.debug('Mint Assets', address, count, tokenUri, account)
        const web3 = await this.getWeb3ContractSafe();
        return receiveTxAsPromise(this.dispatch,
            () => web3.methods.mintAssets(address, count, tokenUri).send({
                from: account
            }),
            'Add new Collection'
        );
    }

    async transferAsset(collection, to, tokenId, account) {
        const web3 = await this.getWeb3ContractSafe();
        return receiveTxAsPromise(this.dispatch,
        () => {
                return web3.methods.transferAsset(collection, to, tokenId).send({
                    from: account
                });
            },
            'Transfer Manufacturer Asset'
        );
    }
}