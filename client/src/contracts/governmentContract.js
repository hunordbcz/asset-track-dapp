import Web3 from "web3";
import GovernmentABI from "./abis/Government.json";
import {connectWeb3} from "../providers/web3Slice";
import {receiveTxAsPromise} from "./providers/transactionsSlice";

export default class GovernmentContract {
    constructor(address, dispatch) {
        this.address = address;
        this.dispatch = dispatch;
    }

    async getWeb3ContractSafe() {
        if (this.web3Contract === undefined) {
            await this.dispatch(connectWeb3());

            const web3Instance = new Web3(window.ethereum);
            this.web3Contract = new web3Instance.eth.Contract(GovernmentABI.abi, this.address);
        }

        return this.web3Contract;
    }

    getWeb3Contract() {
        if (this.web3Contract === undefined) {
            const web3Instance = new Web3(window.ethereum);
            this.web3Contract = new web3Instance.eth.Contract(GovernmentABI.abi, this.address);
        }

        return this.web3Contract;
    }

    async getOwner() {
        return await this.getWeb3Contract().methods.owner().call();
    }

    async isManufacturer(address) {
        if (!Web3.utils.isAddress(address)) {
            return new Promise((resolve, reject) => reject('Invalid address specified'))
        }
        return await this.getWeb3Contract().methods.isManufacturer(address).call()
    }

    async validManufacturer(address) {
        if (!Web3.utils.isAddress(address)) {
            return new Promise((resolve, reject) => reject('Invalid address specified'))
        }

        return new Promise(async (resolve, reject) => {
            await this.getWeb3Contract().getPastEvents('AssignedManufacturer', {
                fromBlock: 'earliest',
                filter: {
                    contractAddress: address
                }
            })
                .then(events => {
                    resolve(events.length > 0);
                })
                .catch(() => {
                    resolve(false)
                })
        });
    }

    async getManufacturerContract(address) {
        if (!Web3.utils.isAddress(address)) {
            return new Promise((resolve, reject) => reject('Invalid address specified'))
        }
        return await this.getWeb3Contract().methods.getManufacturerContract(address).call()
    }

    async getManufacturerContractBySymbol(symbol) {
        return await this.getWeb3Contract().methods.manufacturerBySymbol(symbol).call()
    }

    async newManufacturer(address, name, symbol, from) {
        if (!Web3.utils.isAddress(address)) {
            return new Promise((resolve, reject) => reject('Invalid address specified'))
        }
        const web3 = await this.getWeb3ContractSafe();
        return receiveTxAsPromise(this.dispatch,
            () => web3.methods.newManufacturer(address, name, symbol).send({
                from
            }),
            'Add new Manufacturer'
        );
    }

    async close(who, account) {
        const web3 = await this.getWeb3ContractSafe();
        return receiveTxAsPromise(this.dispatch,
            () => web3.methods.closeManufacturer(who).send({
                from: account
            }),
            'Close Manufacturer'
        );
    }

    async open(who, account) {
        const web3 = await this.getWeb3ContractSafe();
        return receiveTxAsPromise(this.dispatch,
            () => web3.methods.openManufacturer(who).send({
                from: account
            }),
            'Open Manufacturer'
        );
    }
}