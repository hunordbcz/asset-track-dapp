import Web3 from "web3";
import CollectionABI from "./abis/Collection.json";
import {connectWeb3} from "../providers/web3Slice";
import {receiveTxAsPromise} from "./providers/transactionsSlice";

export default class CollectionContract {
    constructor(address, dispatch) {
        this.address = address;
        this.dispatch = dispatch;
    }

    async getWeb3ContractSafe() {
        if (this.web3Contract === undefined) {
            await this.dispatch(connectWeb3());

            const web3Instance = new Web3(window.ethereum);
            this.web3Contract = new web3Instance.eth.Contract(CollectionABI.abi, this.address);
        }

        return this.web3Contract;
    }

    getWeb3Contract() {
        if (this.web3Contract === undefined) {
            const web3Instance = new Web3(window.ethereum);
            this.web3Contract = new web3Instance.eth.Contract(CollectionABI.abi, this.address);
        }

        return this.web3Contract;
    }

    async collectData() {
        const [owner, supply, name, symbol, tokenUri] = await Promise.all([
            this.getOwner(),
            this.getSupply(),
            this.getName(),
            this.getSymbol(),
            this.getBaseUri(),
        ]);

        return {
            owner, supply, name, symbol, tokenUri
        }
    }

    async getAssetURI(tokenId) {
        let tokenURI = await this.getTokenUri(tokenId);
        if (!tokenURI.endsWith(".json")) {
            tokenURI = tokenURI + ".json";
        }
        return tokenURI;
    }

    async transferAsset(from, to, tokenId) {
        const web3 = await this.getWeb3ContractSafe();
        return receiveTxAsPromise(this.dispatch,
            () => web3.methods.transferFrom(from, to, tokenId).send({
                from
            }),
            'Transfer Asset'
        );
    }

    async getAssetOwner(tokenId) {
        return await (await this.getWeb3Contract()).methods.ownerOf(tokenId).call();
    }

    async getOwner() {
        return await (await this.getWeb3Contract()).methods.owner().call();
    }

    async getSupply() {
        return await this.getWeb3Contract().methods.totalSupply().call();
    }

    async getName() {
        return await this.getWeb3Contract().methods.name().call();
    }

    async getSymbol() {
        return await this.getWeb3Contract().methods.symbol().call();
    }

    async getTokenUri(tokenId) {
        return await this.getWeb3Contract().methods.tokenURI(tokenId).call();
    }

    async getBaseUri() {
        return await this.getWeb3Contract().methods.baseUri().call();
    }

    async ownerOf(tokenId) {
        return await this.getWeb3Contract().methods.ownerOf(tokenId).call();
    }

    async name() {
        return await this.getWeb3Contract().methods.name().call();
    }
}