import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import GovernmentABI from "../abis/Government.json";
import {connectWeb3, connectWeb3Eagerly} from "../../providers/web3Slice";
import {errorMessage} from "../../providers/notifierSlice";
import GovernmentContract from "../governmentContract";

const initialState = {
    contract: undefined,
    address: undefined
};

export const initStore = createAsyncThunk(
    'government/initStore', async (isSendTransaction, {getState, dispatch}) => {
        const {web3, government} = getState()
        if (isSendTransaction && !web3.accountConnected) {
            await dispatch(connectWeb3Eagerly())
        } else if (!web3.providerActive) {
            await dispatch(connectWeb3())
        }

        if (government.contract === undefined) {
            const network = GovernmentABI.networks[web3.networkId];
            if (network === undefined) {
                dispatch(errorMessage('Government contract is not deployed on the current network. According to the ABI..'))
                return false;
            }
            dispatch(setAddress(network.address));
            dispatch(setContract(new GovernmentContract(network.address, dispatch)))
        }

        return true;
    });

export const governmentSlice = createSlice({
    name: 'government',
    initialState,
    reducers: {
        setContract: (state, action) => {
            state.contract = action.payload;
        },
        setAddress: (state, action) => {
            state.address = action.payload;
        }
    },

    // extraReducers: (builder) => {
    //     builder
    //         .addCase(getOwner.rejected, (state, action) => {
    //             state.status = 'rejected';
    //             console.error(action)
    //         })
    //         .addCase(connectWeb3.fulfilled, (state) => {
    //             console.log("activating provider")
    //             state.providerActive = true;
    //         })
    //
    //         .addCase(connectWeb3Eagerly.fulfilled, (state) => {
    //             state.accountConnected = true;
    //             state.providerActive = true;
    //         })
    // },
});

export const {setContract, setAddress} = governmentSlice.actions;

export default governmentSlice.reducer;