import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";

const initialState = {
    array: {}
};

export const receiveTxAsPromise = (dispatch, txFunc, description) => {
    return new Promise((resolve, reject) => {
        dispatch(sendTransaction({txFunc, description})).unwrap().then(r => {
            resolve(r)
        }).catch(e => {
            reject(e)
        })
    });
}

const sendTransaction = createAsyncThunk(
    'transactions/sendTransaction',
    async (data, {dispatch, rejectWithValue, fulfillWithValue}) => {
        let txHash;
        return data.txFunc()
            .on('transactionHash', function (hash) {
                console.debug('onTransactionHash', data.description, hash)
                txHash = hash;
                dispatch(onTransactionHash({hash, description: data.description}))

            }).then(result => {
                console.debug('onResult', data.description, txHash)
                dispatch(onReceipt({
                    response: result,
                    txHash: txHash
                }))
                return fulfillWithValue(result)

            }).catch(error => {
                console.debug('onError', error, data.description, txHash)
                dispatch(onError({
                    response: error,
                    txHash: txHash
                }))
                return rejectWithValue(error)
            })
    });

export const transactionsSlice = createSlice({
    name: 'transactions',
    initialState,
    reducers: {
        onTransactionHash: (state, action) => {
            state.array[action.payload.hash] = {
                description: action.payload.description,
                isDone: false,
                isError: false,
                isDismissed: false,
            }
        },
        onReceipt: (state, action) => {
            let returnData = action.payload;
            state.array[returnData.txHash] = {
                ...state.array[returnData.txHash],
                isDone: true,
            }
        },
        onError: (state, action) => {
            let returnData = action.payload;
            state.array[returnData.txHash] = {
                ...state.array[returnData.txHash],
                isDone: true,
                isError: true,
            }
        },
        dismissTxNotificationByHash: (state, action) => {
            let txHash = action.payload;
            state.array[txHash] = {
                ...state.array[txHash],
                isDismissed: true
            }
        },
        removeTxByHash: (state, action) => {
            if (state.array[action.payload].isDone) {
                delete state.array[action.payload]
            }
        }
    },
    extraReducers: (builder) => {
        // builder
        // .addCase(sendTransaction.pending, (state, action) => {
        //     state.status = 'rejected';
        //     console.error(action)
        // })
        // .addCase(connectWeb3.fulfilled, (state) => {
        //     console.log("activating provider")
        //     state.providerActive = true;
        // })
        //
        // .addCase(connectWeb3Eagerly.fulfilled, (state) => {
        //     state.accountConnected = true;
        //     state.providerActive = true;
        // })
    },
});

export const {
    onTransactionHash,
    onReceipt,
    onError,
    dismissTxNotificationByHash,
    removeTxByHash
} = transactionsSlice.actions;

export default transactionsSlice.reducer;