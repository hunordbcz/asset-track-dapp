import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {store} from './providers/store';
import {Provider} from 'react-redux';
import * as serviceWorker from './serviceWorker';
import {SnackbarProvider} from "notistack";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import UseNotifier from "./components/notification/UseNotifier";
import Manufacturer from "./pages/manufacturer/Manufacturer";
import Home from "./pages/Home";
import {ThemeProvider} from "@mui/material";
import {darkTheme} from "./theme"
import TransactionAlert from "./components/notification/TransactionAlert";
import TransactionNotifier from "./components/notification/TransactionNotifier";
import NewCollection from "./components/NewCollection";
import {Layout} from "./Layout";
import Collection from "./pages/collection/Collection";
import NFT from "./pages/collection/NFT";

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <ThemeProvider theme={darkTheme}>
                <SnackbarProvider
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                    }}
                    content={(key, message) => (
                        <TransactionAlert id={key} message={message}/>
                    )}
                >
                    <TransactionNotifier/>
                </SnackbarProvider>
                <SnackbarProvider
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                >
                    <UseNotifier/>
                    <BrowserRouter>
                        <Routes>
                            <Route element={<Layout/>}>
                                <Route index element={<Home/>}/>
                                <Route path="manufacturer/:address" element={<Manufacturer/>}>
                                    <Route index element={<h1>Main page</h1>}/>
                                    <Route path="newCollection" element={<NewCollection/>}/>
                                </Route>
                                <Route path="collection/:address">
                                    <Route index element={<Collection/>}/>
                                    <Route path={":id"} element={<NFT/>}/>
                                </Route>
                                <Route
                                    path="*"
                                    element={
                                        <main style={{padding: "1rem"}}>
                                            <p>There's nothing here! Page not found...</p>
                                        </main>
                                    }
                                />
                            </Route>
                        </Routes>
                    </BrowserRouter>
                </SnackbarProvider>
            </ThemeProvider>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
