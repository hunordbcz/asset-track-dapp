import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Box,
    Button,
    Divider,
    LinearProgress,
    Paper,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow,
    TextField,
    Tooltip,
    Typography
} from "@mui/material";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {errorMessage, successMessage} from "../../providers/notifierSlice";
import {useNavigate} from "react-router-dom";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {initData} from "../../providers/accountSlice";
import ActionIdenticon from "../../components/ActionIdenticon";
import {formatAddress, getDateFromBlock} from "../../utils/Web3Utils";

export default function Government() {

    const currentAccount = useSelector(state => state.account.address);
    const isGovernmentOwner = useSelector(state => state.account.isGovernmentOwner);
    const {address, contract} = useSelector(state => state.government);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [manufacturerName, setManufacturerName] = useState('');
    const [manufacturerSymbol, setManufacturerSymbol] = useState('');
    const [manufacturerAddress, setManufacturerAddress] = useState('');
    const [ready, setReady] = useState(false);
    const [manufacturers, setManufacturers] = useState([]);
    const [filterByName, setFilterByName] = useState('');
    const [filterByOwner, setFilterByOwner] = useState('');
    const [filterByAddress, setFilterByAddress] = useState('');
    const [filterByDate, setFilterByDate] = useState('');

    useEffect(() => {
        updateData();
    }, [contract]);

    const updateData = () => {
        if (contract === undefined) {
            return;
        }

        Promise.all(
            [getEvents()]
        ).then(values => {
            processEvents(values[0])

            setReady(true)
        })
    }

    async function processEvents(events) {
        let manufacturers = []
        let idx = 1;
        for (const obj of events) {
            switch (obj.event) {
                case 'AssignedManufacturer': {
                    manufacturers.push({
                        id: idx++,
                        name: obj.returnValues.name,
                        owner: obj.returnValues.who,
                        address: obj.returnValues.contractAddress,
                        date: await getDateFromBlock(obj.blockNumber)
                    })
                    break;
                }
                case 'OwnershipTransferred': {
                    break;
                }
                default:
                    break;
            }
        }

        setManufacturers(manufacturers);
    }


    function getEvents() {
        return contract.getWeb3Contract().getPastEvents('AssignedManufacturer', {
            fromBlock: 'earliest'
        })
    }

    function createManufacturer() {
        contract.newManufacturer(manufacturerAddress, manufacturerName, manufacturerSymbol, currentAccount)
            .then(result => {
                console.debug(result)
                dispatch(successMessage('Successfully added manufacturer `' + manufacturerName + '`'))
                dispatch(initData())
                setManufacturerAddress('')
                setManufacturerName('')
                updateData();
            })
            .catch(error => {
                dispatch(errorMessage('Failed to create manufacturer.'));
                console.error(error);
            })
    }

    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    return (
        <div>
            <Stack>
                <Typography variant={'h5'}>
                    <b>Government</b>
                </Typography>
                <Typography variant={'subtitle1'}>
                    Address: <ActionIdenticon account={address} size={'small'}/>
                </Typography>
            </Stack>
            <Divider sx={{marginY: '1rem'}}/>
            {ready
                ? (<Box marginY='1rem'>
                    <TableContainer component={Paper}>
                        <Typography sx={{
                            textAlign: 'center'
                        }} variant={'body1'}>Manufacturers</Typography>
                        <Table sx={{minWidth: 650}} aria-label="a dense table">
                            <TableHead>
                                <TableRow
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell>ID</TableCell>
                                    <TableCell>
                                        <TextField label="Manufacturer Name" variant="standard"
                                                   onChange={e => setFilterByName(e.target.value)}/>
                                    </TableCell>
                                    <TableCell>
                                        <TextField label="Owner" variant="standard"
                                                   onChange={e => setFilterByOwner(e.target.value)}/>
                                    </TableCell>
                                    <TableCell>
                                        <TextField label="Added at" variant="standard"
                                                   onChange={e => setFilterByDate(e.target.value)}/>
                                    </TableCell>
                                    <TableCell>
                                        <TextField label="Contract Address" variant="standard"
                                                   onChange={e => setFilterByAddress(e.target.value)}/>
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {manufacturers
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .filter(data =>
                                        (data.name.includes(filterByName) &&
                                            data.owner.includes(filterByOwner) &&
                                            data.address.includes(filterByAddress) &&
                                            data.date.toDateString().includes(filterByDate)))
                                    .map((row) => (
                                        <TableRow
                                            key={row.id}
                                            sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                        >
                                            <TableCell>{row.id}</TableCell>
                                            <TableCell>
                                                {row.name}
                                            </TableCell>
                                            <TableCell>
                                                <ActionIdenticon account={row.owner}/>
                                            </TableCell>
                                            <TableCell>
                                                {row.date.toDateString()}
                                            </TableCell>
                                            <TableCell>
                                                <Tooltip
                                                    title={'Navigate to manufacturer page'}
                                                    arrow
                                                >
                                                    <Button variant={'outlined'}
                                                            onClick={() => navigate(`/manufacturer/` + row.address)}>{formatAddress(row.address)}</Button>
                                                </Tooltip>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 25, 100]}
                        component="div"
                        count={manufacturers.filter(data =>
                            (data.name.includes(filterByName) &&
                                data.owner.includes(filterByOwner) &&
                                data.address.includes(filterByAddress))).length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                    />
                    {
                        isGovernmentOwner
                            ? <div>
                                <Accordion TransitionProps={{unmountOnExit: true}}>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon/>}
                                    >
                                        <Typography>Add a new manufacturer <small>(only owner)</small></Typography>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        <Stack my={2} spacing={2}>
                                            <TextField label="Manufacturer Name" variant="outlined"
                                                       onChange={e => setManufacturerName(e.target.value)}/>
                                            <TextField label="Manufacturer Symbol" variant="outlined"
                                                       onChange={e => setManufacturerSymbol(e.target.value.toUpperCase())}/>
                                            <TextField label="Manufacturer Address" variant="outlined"
                                                       onChange={e => setManufacturerAddress(e.target.value)}/>
                                            <Button variant='outlined'
                                                    onClick={createManufacturer}>createManufacturer</Button>
                                        </Stack>
                                    </AccordionDetails>
                                </Accordion>


                                {/*<Divider sx={{*/}
                                {/*    marginY: '1rem'*/}
                                {/*}}/>*/}
                                {/*<Typography variant={'h6'}>Add a new manufacturer <small>(only owner)</small></Typography>*/}
                                {/*<Stack my={2} spacing={2}>*/}
                                {/*    <TextField label="Manufacturer Name" variant="outlined"*/}
                                {/*               onChange={e => setManufacturerName(e.target.value)}/>*/}
                                {/*    <TextField label="Manufacturer Symbol" variant="outlined"*/}
                                {/*               onChange={e => setManufacturerSymbol(e.target.value)}/>*/}
                                {/*    <TextField label="Manufacturer Address" variant="outlined"*/}
                                {/*               onChange={e => setManufacturerAddress(e.target.value)}/>*/}
                                {/*    <Button variant='outlined' onClick={createManufacturer}>createManufacturer</Button>*/}
                                {/*</Stack>*/}

                            </div>
                            : ''
                    }

                </Box>)
                : <LinearProgress sx={{
                    marginY: '1rem'
                }}/>
            }
        </div>
    )
}