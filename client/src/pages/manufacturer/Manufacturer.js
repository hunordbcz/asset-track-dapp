import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Box,
    Button,
    Chip,
    Divider,
    Grid,
    Paper,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow,
    Tooltip,
    Typography
} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import Web3 from "web3";
import {errorMessage} from "../../providers/notifierSlice";
import {connectWeb3} from "../../providers/web3Slice";
import ManufacturerContract from "../../contracts/manufacturerContract";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import NewCollection from "../../components/NewCollection";
import {formatAddress, sameAddress} from "../../utils/Web3Utils";
import {Block, LockOpen, MoreHoriz} from "@mui/icons-material";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import CancelIcon from "@mui/icons-material/Cancel";
import ActionIdenticon from "../../components/ActionIdenticon";

export default function Manufacturer() {

    const {address} = useParams()
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const isProviderActive = useSelector(state => state.web3.providerActive)
    const accountManufacturerAddress = useSelector(state => state.account.manufacturerAddress);
    const governmentContract = useSelector(state => state.government.contract);
    const isGovernment = useSelector(state => state.account.isGovernmentOwner);
    const currentAccount = useSelector(state => state.account.address);

    const [collections, setCollections] = useState([]);
    const [contract, setContract] = useState(null);
    const [data, setData] = useState({});

    useEffect(() => {
        if (!Web3.utils.isAddress(address)) {
            dispatch(errorMessage("Invalid collection address specified"));
            if (window.history.state && window.history.state.idx > 0) {
                navigate(-1);
            } else {
                navigate('/', {replace: true});
            }
            return;
        }

        const async = async () => {
            if (!isProviderActive) {
                const allGood = await dispatch(connectWeb3());
                if (allGood.error) {
                    return;
                }
            }

            let localContract = new ManufacturerContract(address, dispatch);
            setContract(localContract);
            refreshData(localContract)
                .catch(error => {
                    console.error(error)
                    dispatch(errorMessage('Failed to retrieve contract info'));
                    setData(
                        {
                            ...data,
                            valid: false
                        }
                    )
                })
        }
        async();
    }, [address, dispatch, isProviderActive, navigate, governmentContract, accountManufacturerAddress]);

    const refreshData = async (localContract = contract) => {
        if (localContract === null) {
            return;
        }

        if (governmentContract === undefined) {
            return;
        }
        let valid = await governmentContract.validManufacturer(address);

        (await localContract.getWeb3ContractSafe()).getPastEvents('NewCollection', {
            fromBlock: 'earliest'
        })
            .then(events => {
                const collections = events.map(obj => obj.returnValues);
                collections.forEach((data, idx) => data.id = idx)
                setCollections(collections);
            })
            .catch(error => {
                console.error(error)
            })

        const [name, symbol, nrCollections, isClosed] = await Promise.all([
            localContract.getName(),
            localContract.getSymbol(),
            localContract.getNrCollections(),
            localContract.isClosed()
        ]);

        setData({
            name,
            symbol,
            nrCollections,
            isClosed,
            valid
        })
    }

    const columns = [
        {id: 'id', label: 'ID', minWidth: 90},
        {
            id: 'symbol',
            label: 'Symbol',
            minWidth: 150,
        },
        {
            id: 'name',
            label: 'Name',
            minWidth: 150,
        },
        {
            id: 'contractAddress',
            label: 'Collection Address',
            minWidth: 200,
            format: (value) =>
                <Tooltip
                    title={'Navigate to collection page'}
                    arrow
                >
                    <Button variant={'outlined'}
                            onClick={() => navigate(`/collection/` + value)}>{formatAddress(value)}</Button>
                </Tooltip>
        },
    ];

    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    async function openOrCloseManufacturer() {
        if (!isGovernment || governmentContract === undefined) {
            return;
        }

        if (data.isClosed) {
            await governmentContract.open(address, currentAccount);
        } else {
            await governmentContract.close(address, currentAccount);
        }

        refreshData();
    }

    return (
        <Box>
            <Divider/>
            <Paper sx={{padding: '1rem'}}>
                <Grid
                    container
                    direction="row"
                    justifyContent="space-between"
                    alignItems="flex-start"
                >
                    <Grid item>
                        <Stack>
                            <Typography variant={'h5'}>
                                Manufacturer: <b>{data.name}</b>
                            </Typography>
                            <Typography variant={'subtitle1'}>
                                Symbol: <b>{data.symbol}</b> ┃ Address: <ActionIdenticon account={address}
                                                                                         size={'small'}/>
                            </Typography>
                            <Divider sx={{marginY: '.25rem'}}/>
                            <Typography variant={'caption'}>
                                Existing Collections: <b>{data.nrCollections}</b>
                            </Typography>
                        </Stack>
                    </Grid>
                    <Grid item>
                        <Stack spacing={1}>
                            <Box>
                                Validity check:
                                <Chip
                                    color={data.valid === undefined ? 'primary' : data.valid === true ? 'success' : 'error'}
                                    icon={
                                        data.valid === undefined
                                            ? <MoreHoriz/>
                                            : data.valid === true
                                                ? <CheckCircleIcon/>
                                                : <CancelIcon/>
                                    } label="Legitimate"
                                    sx={{
                                        marginX: '.5rem'
                                    }}
                                />
                                <Chip
                                    color={data.isClosed === undefined ? 'primary' : data.isClosed === false ? 'success' : 'error'}
                                    icon={
                                        data.isClosed === undefined
                                            ? <MoreHoriz/>
                                            : data.isClosed === false
                                                ? <CheckCircleIcon/>
                                                : <CancelIcon/>
                                    }
                                    label={!data.isClosed ? 'Open' : 'Closed'}
                                    onDelete={isGovernment ? openOrCloseManufacturer : null}
                                    deleteIcon={data.isClosed ? <LockOpen/> : <Block/>}
                                />
                            </Box>
                        </Stack>
                    </Grid>
                </Grid>
                <Divider sx={{marginY: '1rem'}}/>
                <TableContainer sx={{maxHeight: 440}}>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                {columns.map((column) => (
                                    <TableCell
                                        key={column.id}
                                        align={column.align}
                                        style={{minWidth: column.minWidth}}
                                    >
                                        {column.label}
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {collections
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {
                                    return (
                                        <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                                            {columns.map((column) => {
                                                const value = row[column.id];
                                                return (
                                                    <TableCell key={column.id} align={column.align}>
                                                        {column.format
                                                            ? column.format(value)
                                                            : value}
                                                    </TableCell>
                                                );
                                            })}
                                        </TableRow>
                                    );
                                })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={collections.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </Paper>

            {sameAddress(accountManufacturerAddress, address)
                ?
                <Accordion TransitionProps={{unmountOnExit: true}}>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon/>}
                    >
                        <Typography>Add a new collection <small>(only owner)</small></Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <NewCollection contract={contract} onSuccess={refreshData}/>
                    </AccordionDetails>
                </Accordion>
                : null
            }
        </Box>
    )
}