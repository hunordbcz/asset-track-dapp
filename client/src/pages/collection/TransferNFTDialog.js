import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField} from "@mui/material";
import {useState} from "react";
import Web3 from "web3";
import {useDispatch} from "react-redux";
import {errorMessage} from "../../providers/notifierSlice";

export default function TransferNFTDialog({serialNumber, onTransfer, children}) {

    const dispatch = useDispatch();
    const [open, setOpen] = useState(false);
    const [receiver, setReceiver] = useState('');

    const handleTransfer = () => {
        if (!isValid(receiver)) {
            dispatch(errorMessage("Invalid receiver address"));
            return;
        }

        if (typeof onTransfer === 'function') {
            onTransfer(receiver)
        }

        setOpen(false);
    }

    const isValid = (address) => {
        return Web3.utils.isAddress(address)
    }

    return (
        <div>
            <Button variant="outlined" onClick={() => setOpen(true)}>
                {children}
            </Button>
            <Dialog open={open}>
                <DialogTitle>Transfer asset <b>{serialNumber}</b></DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        This will transfer the ownership of the asset to the below specified address:
                    </DialogContentText>
                    <br/>
                    <TextField
                        autoFocus
                        label="Receiver Address"
                        fullWidth
                        variant="standard"
                        onChange={e => setReceiver(e.target.value)}
                        error={!isValid(receiver) && receiver.length !== 0}
                    />
                </DialogContent>
                <DialogActions>
                    <Button variant={'outlined'} color={'error'} onClick={() => setOpen(false)}>Cancel</Button>
                    <Button variant={'outlined'} color={'primary'} onClick={handleTransfer}>Transfer</Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}