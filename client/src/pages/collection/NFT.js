import {useNavigate, useParams} from "react-router-dom";
import {connectWeb3} from "../../providers/web3Slice";
import CollectionContract from "../../contracts/collectionContract";
import {errorMessage, infoMessage, successMessage} from "../../providers/notifierSlice";
import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useState} from "react";
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Box,
    Chip,
    Divider,
    Grid,
    Paper,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow,
    TextField,
    Tooltip,
    Typography
} from "@mui/material";
import {MoreHoriz} from "@mui/icons-material";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import CancelIcon from "@mui/icons-material/Cancel";
import {formatAddress, getDateFromBlock, sameAddress, validCollectionOverall} from "../../utils/Web3Utils";
import TransferNFTDialog from "./TransferNFTDialog";
import {IPFS} from "../../utils/IPFSHandler";
import ManufacturerContract from "../../contracts/manufacturerContract";
import ActionIdenticon from "../../components/ActionIdenticon";
import Web3 from "web3";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

export default function NFT() {
    const dispatch = useDispatch();

    const isProviderActive = useSelector(state => state.web3.providerActive)
    const governmentContract = useSelector(state => state.government.contract);
    const accountAddress = useSelector(state => state.account.address);
    const currentManufacturerAddress = useSelector(state => state.account.manufacturerAddress);
    const {address, id} = useParams()
    const navigate = useNavigate();

    const [manufacturerContract, setManufacturerContract] = useState(null);
    const [collectionContract, setCollectionContract] = useState(null);
    const [legit, setLegit] = useState(null);
    const [ownership, setOwnership] = useState({
        currentAccount: false,
        manufacturer: false,
        transferableThroughManufacturer: false
    });
    const [data, setData] = useState({});
    const [image, setImage] = useState({
        file: null,
        blob: null,
    });
    const [transfers, setTransfers] = useState([]);
    const [filterBySender, setFilterBySender] = useState('');
    const [filterByReceiver, setFilterByReceiver] = useState('');
    const [filterByTxHash, setFilterByTxHash] = useState('');
    const [filterByDate, setFilterByDate] = useState('');

    useEffect(() => {
        refreshState();
        return clearState();
    }, [address, id, governmentContract, accountAddress]);

    const clearState = () => {
        if (image.blob !== null) {
            URL.revokeObjectURL(image.blob)
        }
    }

    const loadContract = async () => {
        if (!isProviderActive) {
            const allGood = await dispatch(connectWeb3());
            if (allGood.error) {
                console.error(allGood())
                return;
            }
        }

        let localContract = new CollectionContract(address, dispatch);
        setCollectionContract(localContract);
        return localContract;
    }

    const refreshData = async (contract = collectionContract) => {
        // Set other data
        const [tokenURI, owner] = await Promise.all([
            contract.getAssetURI(id),
            contract.getAssetOwner(id)
        ]);
        const tokenData = JSON.parse(await IPFS.getByCIDasString(tokenURI));
        setData({
            ...tokenData,
            owner
        });
        console.debug("NFT data loaded: ", tokenData)
        const uint8Image = await IPFS.getByCID(tokenData.image);
        const imageFile = new File([uint8Image], 'image.png', {type: 'image/png'})

        setImage({
            file: imageFile,
            blob: URL.createObjectURL(imageFile)
        })

        //Set ownership
        const manufacturerAddress = await contract.getOwner();
        setManufacturerContract(new ManufacturerContract(manufacturerAddress, dispatch));
        setOwnership({
            currentAccount: sameAddress(owner, accountAddress),
            manufacturer: sameAddress(owner, manufacturerAddress),
            transferableThroughManufacturer: sameAddress(owner, currentManufacturerAddress)
        })

        dispatch(connectWeb3());
        const web3 = new Web3(window.ethereum);

        contract.getWeb3Contract().getPastEvents('Transfer', {
            fromBlock: 'earliest',
            filter: {
                tokenId: id
            }
        }).then(async events => {
            const transfers = [];
            for (const event of events) {
                const data = event.returnValues;
                data.date = await getDateFromBlock(event.blockNumber, web3);
                data.txHash = event.transactionHash
                transfers.push(data);
            }

            transfers.sort((a, b) => a.date < b.date)
            setTransfers(transfers)
        })
    }

    const refreshState = async () => {
        setLegit(await validCollectionOverall(governmentContract, address, dispatch));
        const localContract = await loadContract();
        refreshData(localContract)
            .catch(error => {
                console.error(error)
                dispatch(errorMessage('Failed to retrieve info'));
            })
    }


    function transferNft(to) {
        collectionContract.transferAsset(accountAddress, to, id)
            .then(result => {
                dispatch(successMessage("Asset successfully transferred"))
                refreshState()
            })
            .catch(error => {
                dispatch(errorMessage("Couldn't transfer asset"));
            })
    }

    function transferNftAsManufacturer(to) {
        manufacturerContract.transferAsset(address, to, id, accountAddress)
            .then(result => {
                dispatch(successMessage("Asset successfully transferred"))
                refreshState()
            })
            .catch(error => {
                dispatch(errorMessage("Couldn't transfer asset"));
            })
    }

    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    return (
        <div>
            <Paper sx={{
                padding: '1rem'
            }}>
                <Grid
                    container
                    direction="row"
                    justifyContent="space-between"
                    alignItems="flex-start"
                >
                    <Grid item>
                        <Stack>
                            <Typography variant={'h5'}>
                                Asset: <b>{data.name}</b>
                            </Typography>
                            <Typography variant={'caption'}>
                                SN: {data.serialNumber}
                            </Typography>
                            <Divider sx={{marginY: '.25rem'}}/>
                            <Typography>
                                <small>Description:</small> {data.description}
                            </Typography>
                        </Stack>
                    </Grid>
                    <Grid item>
                        <Stack spacing={1}>
                            <Box sx={{
                                textAlign: 'right'
                            }}>
                                Validity check:
                                <Chip color={legit === null ? 'primary' : legit === true ? 'success' : 'error'}
                                      icon={
                                          legit === null
                                              ? <MoreHoriz/>
                                              : legit === true
                                                  ? <CheckCircleIcon/>
                                                  : <CancelIcon/>
                                      } label="Legitimate"
                                      sx={{
                                          marginX: '.5rem'
                                      }}
                                />
                                {ownership.currentAccount ?
                                    <Chip color={'primary'} label="Connected account owns this"/> : null}
                                {ownership.manufacturer ?
                                    <Chip color={'primary'} label="Manufacturer owns this"/> : null}
                            </Box>
                            <Box sx={{
                                textAlign: 'right'
                            }}>

                                {legit === true ?
                                    <Chip label={<div>
                                        See collection: <b>{formatAddress(address)}</b>
                                    </div>} onClick={() => navigate('/collection/' + address)}/>
                                    : null
                                }
                            </Box>
                        </Stack>
                    </Grid>
                </Grid>
                <Divider sx={{marginY: '1rem'}}/>
                <Box sx={{
                    textAlign: 'center',
                    height: '300px'
                }}>
                    <img
                        src={image.blob}
                        style={{
                            maxHeight: '100%',
                            maxWidth: '100%',
                        }}
                    />
                </Box>
                <Divider sx={{marginY: '1rem'}}/>

                <Grid container
                      direction="row"
                      justifyContent="space-between"
                      alignItems="flex-start">
                    <Grid item>
                        <Typography variant={'subtitle1'}>
                            Attributes:
                        </Typography>
                        {data.attributes !== undefined ?
                            data.attributes.map(attr =>
                                <Chip key={attr.trait_type} label={
                                    <div>
                                        {attr.trait_type}: <b>{attr.value}</b>
                                    </div>
                                } sx={{
                                    margin: '.25rem'
                                }}/>
                            )
                            : null
                        }
                    </Grid>
                    <Grid item>
                        {ownership.currentAccount ?
                            <TransferNFTDialog serialNumber={data.serialNumber} onTransfer={to => transferNft(to)}>
                                Transfer Asset
                            </TransferNFTDialog> : null}
                        {ownership.transferableThroughManufacturer ?
                            <TransferNFTDialog serialNumber={data.serialNumber}
                                               onTransfer={to => transferNftAsManufacturer(to)}>
                                Transfer Asset as Manufacturer
                            </TransferNFTDialog> : null}
                    </Grid>
                </Grid>
                <Divider sx={{marginY: '1rem'}}/>
                <Accordion TransitionProps={{unmountOnExit: true}}>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon/>}
                    >
                        <Typography>List of Transfers</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <TableContainer component={Paper}>
                            <Typography sx={{
                                textAlign: 'center'
                            }} variant={'body1'}>Manufacturers</Typography>
                            <Table sx={{minWidth: 650}} aria-label="a dense table">
                                <TableHead>
                                    <TableRow
                                        sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                        <TableCell>
                                            <TextField label="Sender" variant="standard"
                                                       onChange={e => setFilterBySender(e.target.value)}/>
                                        </TableCell>
                                        <TableCell>
                                            <TextField label="Receiver" variant="standard"
                                                       onChange={e => setFilterByReceiver(e.target.value)}/>
                                        </TableCell>
                                        <TableCell>
                                            <TextField label="Date" variant="standard"
                                                       onChange={e => setFilterByDate(e.target.value)}/>
                                        </TableCell>
                                        <TableCell>
                                            <TextField label="TX Hash" variant="standard"
                                                       onChange={e => setFilterByTxHash(e.target.value)}/>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {transfers
                                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                        .filter(data =>
                                            (data.from.includes(filterBySender) &&
                                                data.to.includes(filterByReceiver) &&
                                                data.txHash.includes(filterByTxHash) &&
                                                data.date.toDateString().includes(filterByDate)))
                                        .map((row, idx) => (
                                            <TableRow
                                                key={idx}
                                                sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                            >
                                                <TableCell>
                                                    <ActionIdenticon account={row.from}/>
                                                </TableCell>
                                                <TableCell>
                                                    <ActionIdenticon account={row.to}/>
                                                </TableCell>
                                                <TableCell>
                                                    {row.date.toDateString()}
                                                </TableCell>
                                                <TableCell>
                                                    <Tooltip title={'Copy transaction hash'}>
                                                        <Chip onClick={() => {
                                                            navigator.clipboard.writeText(row.txHash)
                                                            dispatch(infoMessage("Copied transaction hash to clipboard"))
                                                        }} variant={"outlined"} label={formatAddress(row.txHash)}/>
                                                    </Tooltip>
                                                </TableCell>
                                            </TableRow>
                                        ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                        <TablePagination
                            rowsPerPageOptions={[5, 10, 25, 100]}
                            component="div"
                            count={transfers.filter(data =>
                                (data.from.includes(filterBySender) &&
                                    data.to.includes(filterByReceiver) &&
                                    data.txHash.includes(filterByTxHash) &&
                                    data.date.toDateString().includes(filterByDate))).length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </AccordionDetails>
                </Accordion>
            </Paper>

        </div>
    )
}