import {useNavigate, useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {connectWeb3} from "../../providers/web3Slice";
import Web3 from "web3";
import {errorMessage, successMessage} from "../../providers/notifierSlice";
import CollectionContract from "../../contracts/collectionContract";
import {
    Accordion,
    AccordionDetails,
    AccordionSummary, Backdrop,
    Box,
    Button,
    Chip, CircularProgress,
    Divider,
    Grid,
    Paper,
    Stack,
    TextField,
    Typography
} from "@mui/material";
import ManufacturerContract from "../../contracts/manufacturerContract";
import {IPFS} from "../../utils/IPFSHandler";
import UserNFTs from "./UserNFTs";
import {formatAddress, validCollectionOverall} from "../../utils/Web3Utils";
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CancelIcon from '@mui/icons-material/Cancel';
import {MoreHoriz} from "@mui/icons-material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ActionIdenticon from "../../components/ActionIdenticon";

export default function Collection() {
    const {address} = useParams()
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const isProviderActive = useSelector(state => state.web3.providerActive)
    const manufacturerAddress = useSelector(state => state.account.manufacturerAddress);
    const account = useSelector(state => state.web3.account);
    const governmentContract = useSelector(state => state.government.contract);

    const [collectionContract, setCollectionContract] = useState(null);
    const [manufacturerContract, setManufacturerContract] = useState(null);
    const [count, setCount] = useState(0);
    const [blockchainData, setBlockchainData] = useState({});
    const [ipfsData, setIpfsData] = useState({});
    const [attributes, setAttributes] = useState({});
    const [validContract, setValidContract] = useState(null);
    const [refresh, setRefresh] = useState(true);
    const [awaitingTx, setAwaitingTx] = useState(false);

    useEffect(() => {
        if (!Web3.utils.isAddress(address)) {
            dispatch(errorMessage("Invalid collection address specified"));
            if (window.history.state && window.history.state.idx > 0) {
                navigate(-1);
            } else {
                navigate('/', {replace: true});
            }
            return;
        }

        const async = async () => {
            if (!isProviderActive) {
                const allGood = await dispatch(connectWeb3());
                if (allGood.error) {
                    return;
                }
            }

            let localContract = new CollectionContract(address, dispatch);
            setCollectionContract(localContract);
            refreshData(localContract)
                .catch(error => {
                    console.debug('strange error', error)
                    dispatch(errorMessage('Failed to retrieve collectionContract info'));
                })
        }

        async();
    }, [address, dispatch, isProviderActive, navigate, governmentContract]);

    const isOwnerOfCollection = () => {
        return manufacturerAddress === blockchainData.owner
    }

    const refreshData = async (localContract = collectionContract) => {
        if (governmentContract === undefined) {
            return;
        }
        validCollectionOverall(governmentContract, address, dispatch)
            .then(async isValid => {
                setValidContract(isValid)
                if (isValid) {
                    await loadBlockchainData(localContract)
                        .then(data => {
                            loadIPFSData(data.tokenUri);
                        });
                }

                refreshChild();
            })
            .catch(() => setValidContract(false))
    }


    function refreshChild() {
        setRefresh(!refresh)
    }

    const loadIPFSData = async (uri) => {
        if (uri === undefined) {
            return;
        }

        const json = await IPFS.getByCIDasString(uri + "/collection.json");
        setIpfsData(JSON.parse(json));
    }

    function loadBlockchainData(localContract = collectionContract) {
        return new Promise(async (resolve, reject) => {
            if (localContract === null) {
                reject(null)
                return;
            }

            const {owner, supply, name, symbol, tokenUri, isPaused} = await localContract.collectData();
            const manufacturerContract = new ManufacturerContract(owner, dispatch);
            setManufacturerContract(manufacturerContract);
            const [manufacturerSymbol, manufacturerIsClosed, ipfsDag] = await Promise.all([
                manufacturerContract.getSymbol(),
                manufacturerContract.isClosed(),
                IPFS.getDag(tokenUri)
            ]);

            const imageCID = ipfsDag.value.Links
                .find(link => link.Name.includes('image'))
                .Hash.toString()

            let data = {
                owner, supply, name, symbol, tokenUri, isPaused, imageCID, manufacturerSymbol, manufacturerIsClosed
            };
            setBlockchainData(data)
            resolve(data);
        })
    }

    async function mintNewSupply() {
        if (count <= 0) {
            dispatch(errorMessage("Invalid number set to mint"));
            return;
        }
        setAwaitingTx(true);
        const imageCID = (await IPFS.getDag(blockchainData.tokenUri)).value.Links
            .find(link => link.Name.includes('image'))
            .Hash.toString()

        let possibleFileNames = ['collection.json', 'image.png'];
        for (let i = 0; i < blockchainData.supply; i++) {
            possibleFileNames.push(i + ".json");
        }
        const folderData = await IPFS.dagToJson(blockchainData.tokenUri, fileName => {
            return possibleFileNames.includes(fileName);
        });

        const content = {
            image: imageCID,
            description: ipfsData.description,
            manufacturer: ipfsData.manufacturer,
            attributes: Object.entries(attributes).map(([trait_type, value]) => {
                return {
                    trait_type,
                    value
                }
            })
        };

        for (let i = 0; i < count; i++) {
            const number = i + parseInt(blockchainData.supply);
            folderData.push({
                path: number + ".json",
                content: JSON.stringify({
                    ...content,
                    name: blockchainData.name,
                    serialNumber: blockchainData.manufacturerSymbol + '-' + blockchainData.symbol + '-' + number + "-"
                })
            })
        }

        console.debug("Adding new data", folderData);

        const newCid = (await IPFS.addFiles(folderData)).find(obj => obj.path === '').cid.toString();
        console.debug('new cid:', newCid)

        manufacturerContract.mintAssets(address, count, newCid, account)
            .then(result => {
                console.debug("mintAssetsResult: ", result);
                dispatch(successMessage("Successfully minted " + count + " new assets"))
                refreshData();
            })
            .catch(error => {
                console.error(error)
            })
            .finally(() => {
                setAwaitingTx(false);
            })
    }

    function updateAttribute(attr, value) {
        const localAttributes = attributes;
        if (value === "") {
            delete localAttributes[attr];
        } else {
            localAttributes[attr] = value;
        }
        setAttributes(localAttributes);
    }

    return (
        <div>
            <Paper
                sx={{
                    padding: '1rem'
                }}
            >
                <Grid
                    container
                    direction="row"
                    justifyContent="space-between"
                    alignItems="flex-start"
                >
                    <Grid item>
                        <Stack>
                            <Typography variant={'h5'}>
                                Collection: <b>{blockchainData.name}</b>
                            </Typography>
                            <Typography variant={'subtitle1'}>
                                Symbol: <b>{blockchainData.symbol}</b> ┃ Address: <ActionIdenticon account={address}
                                                                                                   size={'small'}/>
                            </Typography>
                            <Divider sx={{marginY: '.25rem'}}/>
                            <Typography variant={'caption'}>
                                Existing Supply: <b>{blockchainData.supply}</b>
                            </Typography>
                        </Stack>
                    </Grid>
                    <Grid item>
                        <Stack spacing={1}>
                            <Box>
                                Validity check:
                                <Chip
                                    color={validContract === null ? 'primary' : validContract === true ? 'success' : 'error'}
                                    icon={
                                        validContract === null
                                            ? <MoreHoriz/>
                                            : validContract === true
                                                ? <CheckCircleIcon/>
                                                : <CancelIcon/>
                                    } label="Legitimate"
                                    sx={{
                                        marginX: '.5rem'
                                    }}
                                />
                                <Chip
                                    color={blockchainData.manufacturerIsClosed === undefined ? 'primary' : blockchainData.manufacturerIsClosed === false ? 'success' : 'error'}
                                    icon={
                                        blockchainData.manufacturerIsClosed === undefined
                                            ? <MoreHoriz/>
                                            : blockchainData.manufacturerIsClosed === false
                                                ? <CheckCircleIcon/>
                                                : <CancelIcon/>
                                    } label={!blockchainData.manufacturerIsClosed ? 'Open' : 'Closed'}
                                />
                            </Box>
                            <Box sx={{
                                textAlign: 'right'
                            }}>
                                {validContract === true ?
                                    <Chip label={<div>
                                        See manufacturer: <b>{formatAddress(blockchainData.owner)}</b>
                                    </div>} onClick={() => navigate('/manufacturer/' + blockchainData.owner)}/>
                                    : null
                                }
                            </Box>
                        </Stack>
                    </Grid>
                </Grid>
                <Divider sx={{marginY: '1rem'}}/>

                <Accordion TransitionProps={{unmountOnExit: true}}>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon/>}
                    >
                        <Typography>Owned Assets <i>(by current account)</i></Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <UserNFTs collectionContract={collectionContract} accountAddress={account} nftData={{
                            name: blockchainData.name,
                            imageCID: blockchainData.imageCID,
                            sn: id => blockchainData.manufacturerSymbol + '-' + blockchainData.symbol + '-' + id + '-'
                        }} refresh={refresh}/>
                    </AccordionDetails>
                </Accordion>

                {/*<Accordion TransitionProps={{unmountOnExit: true}}>*/}
                {/*    <AccordionSummary*/}
                {/*        expandIcon={<ExpandMoreIcon/>}*/}
                {/*    >*/}
                {/*        <Typography>Activity</Typography>*/}
                {/*    </AccordionSummary>*/}
                {/*    <AccordionDetails>*/}
                {/*        Insert table with activity*/}
                {/*    </AccordionDetails>*/}
                {/*</Accordion>*/}

                {isOwnerOfCollection() ?
                    <Box>
                        <Divider sx={{marginY: '1rem'}}>Only visible by manufacturer owner below </Divider>
                        <Accordion TransitionProps={{unmountOnExit: true}}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon/>}
                            >
                                <Typography>Mint new Assets</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Typography variant={'caption'}>Set the properties</Typography>
                                <br/>
                                {ipfsData.attributes ? ipfsData.attributes.map((attr, index) =>
                                    <TextField key={index} label={attr}
                                               onChange={e => updateAttribute(attr, e.target.value)}
                                               variant={'standard'}
                                               sx={{
                                                   margin: '.5rem'
                                               }}
                                    />
                                ) : null}
                                <TextField
                                    id="outlined-number"
                                    label="Nr to mint"
                                    type="number"
                                    onChange={event => setCount(event.target.value)}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    sx={{
                                        margin: '.5rem'
                                    }}
                                />
                                <Button fullWidth size={'large'} variant={'outlined'}
                                        onClick={mintNewSupply}>Mint</Button>

                            </AccordionDetails>
                        </Accordion>

                        <Accordion TransitionProps={{unmountOnExit: true}}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon/>}
                            >
                                <Typography>Manufacturer's Assets</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <UserNFTs collectionContract={collectionContract} accountAddress={manufacturerAddress}
                                          nftData={{
                                              name: blockchainData.name,
                                              imageCID: blockchainData.imageCID,
                                              sn: id => blockchainData.manufacturerSymbol + '-' + blockchainData.symbol + '-' + id + '-'
                                          }} refresh={refresh}/>
                            </AccordionDetails>
                        </Accordion>
                    </Box>
                    : null
                }
            </Paper>
            <Backdrop
                sx={{color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1}}
                open={awaitingTx}
                // onClick={handleClose}
            >
                <CircularProgress color="inherit"/>
            </Backdrop>
        </div>
    )
}