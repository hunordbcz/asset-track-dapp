import {Box, Button, Card, CardActions, CardContent, Grid, Skeleton, Typography} from "@mui/material";
import {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";

export default function UserNFTs({collectionContract, accountAddress, nftData, refresh}) {

    const navigate = useNavigate();
    const [ownedNFTs, setOwnedNFTs] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        if (collectionContract !== null) {
            refreshData()
        }

    }, [collectionContract, accountAddress, refresh]);

    const refreshData = async () => {
        setLoading(true);

        const receivedNFTsPromise = collectionContract.getWeb3Contract().getPastEvents('Transfer', {
            fromBlock: 'earliest',
            filter: {
                to: accountAddress
            }
        });

        const sentNFTsPromise = collectionContract.getWeb3Contract().getPastEvents('Transfer', {
            fromBlock: 'earliest',
            filter: {
                from: accountAddress
            }
        });

        let [receivedNFTs, sentNFTs] = await Promise.all([receivedNFTsPromise, sentNFTsPromise])
        receivedNFTs = receivedNFTs.map(event => event.returnValues.tokenId)
        sentNFTs = sentNFTs.map(event => event.returnValues.tokenId)

        setOwnedNFTs(receivedNFTs.filter(id => !sentNFTs.includes(id)))
        setLoading(false);
    }

    return (
        <Box>
            {loading
                ?
                <Box>
                    <Skeleton animation="wave"/>
                    <Skeleton animation="wave"/>
                    <Skeleton animation="wave"/>
                </Box>
                : (ownedNFTs.length > 0) ?
                    <Grid container spacing={2}>
                        {ownedNFTs.map(nft =>
                            <Grid item xs={4} key={nft}>
                                <Card sx={{
                                    minWidth: 275
                                }}>
                                    <CardContent>
                                        <Typography sx={{fontSize: 14}} color="text.secondary" gutterBottom>
                                            Name: <b>{nftData.name}</b>
                                        </Typography>
                                        <Typography variant="h6">
                                            SN: <b>{nftData.sn(nft)}</b>
                                        </Typography>
                                        <Typography variant="body2" color="text.secondary">
                                            The SN contains only the prefix of the full serial number.
                                        </Typography>
                                    </CardContent>
                                    <CardActions>
                                        <Button variant={'outlined'} size="small" onClick={() => navigate(nft)}>
                                            See Details
                                        </Button>
                                    </CardActions>
                                </Card>
                            </Grid>
                        )}
                    </Grid>
                    : 'No assets owned'
            }
        </Box>
    )
}