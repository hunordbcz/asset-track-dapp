import {
    Chip,
    CircularProgress,
    Grid,
    IconButton,
    InputAdornment,
    LinearProgress,
    Paper,
    Stack,
    Step,
    StepLabel,
    Stepper,
    TextField,
    Tooltip,
    Typography
} from "@mui/material";
import React, {useState} from "react";
import {useNavigate} from "react-router-dom";
import Government from "./government/Government";
import DoubleArrowIcon from '@mui/icons-material/DoubleArrow';
import Web3 from "web3";
import {useDispatch, useSelector} from "react-redux";
import CollectionContract from "../contracts/collectionContract";
import {isEmptyAddress} from "../utils/Web3Utils";
import ManufacturerContract from "../contracts/manufacturerContract";
import ActionIdenticon from "../components/ActionIdenticon";

export default function Home() {

    const [manufacturer, setManufacturer] = useState({
        address: '',
        valid: true
    });

    const [collection, setCollection] = useState({
        address: '',
        valid: true
    });
    const [loading, setLoading] = useState({
        manufacturer: false,
        collection: false
    });
    const governmentContract = useSelector(state => state.government.contract);
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const updateManufacturerAddress = address => {
        if (address.length === 0) {
            setManufacturer({
                address,
                valid: true
            })
            return;
        }
        setLoading({
            ...loading,
            manufacturer: true
        })

        let validAddress = Web3.utils.isAddress(address);
        if (!validAddress) {
            setLoading({
                ...loading,
                manufacturer: false
            })
            setManufacturer({
                address,
                valid: false
            })
            return;
        }

        isValidManufacturerByContract(address)
            .then(isValid => {
                setManufacturer({
                    address,
                    valid: isValid
                })
            })
            .finally(() => setLoading({
                ...loading,
                manufacturer: false
            }))
    }

    const isValidManufacturerByContract = address => {
        return new Promise((resolve, reject) => {
            governmentContract.validManufacturer(address)
                .then(isValid => {
                    resolve(isValid);
                })
                .catch(() => {
                    resolve(false);
                })
        });
    }

    const updateCollectionAddress = address => {
        if (address.length === 0) {
            setCollection({
                address,
                valid: true
            })
            return;
        }
        setLoading({
            ...loading,
            collection: true
        })

        let validAddress = Web3.utils.isAddress(address);
        if (!validAddress) {
            setLoading({
                ...loading,
                collection: false
            })
            setCollection({
                address,
                valid: false
            })
            return;
        }

        const collectionContract = new CollectionContract(address, dispatch);
        collectionContract.getOwner()
            .then(manufacturerAddress => {
                isValidManufacturerByContract(manufacturerAddress)
                    .then(isValid => {
                        setCollection({
                            address,
                            valid: isValid
                        })
                    })
                    .finally(() => setLoading({
                        ...loading,
                        collection: false
                    }))
            })
            .catch(() => {
                setCollection({
                    address,
                    valid: false
                })
            })
            .finally(() => setLoading({
                ...loading,
                collection: false
            }))
    }

    const [serialNumber, setSerialNumber] = useState({
        value: '',
        valid: false,
        manufacturerSymbol: '',
        collectionSymbol: '',
        id: ''
    });

    function updateSerialNumber(value) {
        setCurrentStep(-1);
        setSteps([{
            title: 'Manufacturer',
        }, {
            title: 'Collection',
        }, {
            title: 'Asset'
        }]);


        const serialNumberRegex = new RegExp('([A-Za-z\\d]{3,})-([A-Za-z\\d]{3,})-(\\d+)-');
        const regexResult = serialNumberRegex.exec(value);
        if (regexResult === null) {
            setSerialNumber({
                ...serialNumber,
                value,
                valid: false
            });
            return;
        }

        const [, manufacturerSymbol, collectionSymbol, id] = regexResult;
        const newVar = {
            value,
            valid: true,
            manufacturerSymbol,
            collectionSymbol,
            id
        };
        setSerialNumber(newVar);
        checkSerialNumber(manufacturerSymbol, collectionSymbol, id);
    }

    const checkSerialNumber = async (manufacturerSymbol, collectionSymbol, id) => {
        let currentStep = 0;
        setCurrentStep(currentStep);
        setSteps(emptySteps);
        const manufacturerAddress = await governmentContract.getManufacturerContractBySymbol(manufacturerSymbol);
        if (isEmptyAddress(manufacturerAddress)) {
            steps[currentStep].error = 'Non-existent'
            setSteps(steps)
            return;
        }
        steps[currentStep].address = manufacturerAddress;
        steps[currentStep].link = '/manufacturer/' + manufacturerAddress;

        setSteps(steps)
        setCurrentStep(++currentStep);
        const collectionAddress = await new ManufacturerContract(manufacturerAddress, dispatch).getCollectionContractBySymbol(collectionSymbol);
        if (isEmptyAddress(collectionAddress)) {
            steps[currentStep].error = 'Non-existent'
            setSteps(steps)
            return;
        }
        steps[currentStep].address = collectionAddress;
        steps[currentStep].link = '/collection/' + collectionAddress;

        setSteps(steps)
        setCurrentStep(++currentStep);
        const supply = await new CollectionContract(collectionAddress, dispatch).getSupply();
        if (parseInt(id) >= parseInt(supply)) {
            steps[currentStep].error = 'Exceeds existing supply'
        }
        steps[currentStep].link = '/collection/' + collectionAddress + "/" + id;
        setSteps(steps)
        setCurrentStep(++currentStep);
    }

    const emptySteps = [{
        title: 'Manufacturer',
    }, {
        title: 'Collection',
    }, {
        title: 'Asset'
    }];

    const [steps, setSteps] = useState(emptySteps);
    const [currentStep, setCurrentStep] = useState(-1);

    return (
        <Stack spacing={2} sx={{
            marginBottom: '2rem'
        }}>
            <Grid
                container
                direction="row"
                justifyContent="center"
                alignItems="center"
                marginBottom={25}
            >
                <Stack>
                    <Typography variant={'h3'}>Serial Number Check</Typography>
                    <TextField
                        placeholder="ABC-XYZ-153-..."
                        onChange={e => updateSerialNumber(e.target.value)}
                        error={!serialNumber.valid && serialNumber.value.length > 0}
                        variant={'standard'}
                        sx={{
                            marginTop: '2rem',
                        }}
                    />
                </Stack>
                <Stepper activeStep={currentStep} sx={{
                    marginLeft: '2.5rem'
                }} orientation={'vertical'}>
                    {steps.map((label, index) => {
                        const labelProps = {};
                        if (label.error) {
                            labelProps.optional = (
                                <Typography variant="caption" color="error">
                                    {label.error}
                                </Typography>
                            );

                            labelProps.error = true;
                        } else if (label.address) {
                            labelProps.optional = (
                                <ActionIdenticon account={label.address} onClick={() => navigate(label.link)}
                                                 tooltipMessage='View'/>
                            );
                        } else if (label.link) {
                            labelProps.optional = (
                                <Chip variant={'outlined'} color={'primary'} onClick={() => navigate(label.link)}
                                      label={'View'}/>
                            );
                        } else if (index === currentStep) {
                            labelProps.optional = (
                                <LinearProgress/>
                            );
                        }

                        return (
                            <Step key={label.title}>
                                <StepLabel {...labelProps}>{label.title}</StepLabel>
                            </Step>
                        );
                    })}
                </Stepper>
            </Grid>
            <Paper sx={{
                padding: '1rem'
            }}>
                <Stack>
                    <TextField
                        label="See Manufacturer Details"
                        onChange={e => updateManufacturerAddress(e.target.value)}
                        error={!manufacturer.valid}
                        color={manufacturer.address.length > 0 ? 'success' : null}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    {loading.manufacturer
                                        ? <CircularProgress color="inherit" size={20}/>
                                        : <Tooltip title={'See details'}>
                                            <IconButton
                                                disabled={!manufacturer.valid && manufacturer.address.length > 0}
                                                onClick={() => {
                                                    if (manufacturer.address.length > 0) {
                                                        navigate('/manufacturer/' + manufacturer.address)
                                                    }
                                                }}
                                            >
                                                <DoubleArrowIcon disabled={!manufacturer.valid}/>
                                            </IconButton>
                                        </Tooltip>
                                    }
                                </InputAdornment>
                            ),
                        }}
                        variant="outlined"
                    />
                </Stack>
            </Paper>
            <Paper sx={{
                padding: '1rem'
            }}>
                <Stack>
                    <TextField
                        label="See Collection Details"
                        onChange={e => updateCollectionAddress(e.target.value)}
                        error={!collection.valid}
                        color={collection.address.length > 0 ? 'success' : null}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    {loading.collection
                                        ? <CircularProgress color="inherit" size={20}/>
                                        : <Tooltip title={'See details'}>
                                            <IconButton
                                                disabled={!collection.valid && collection.address.length > 0}
                                                onClick={() => {
                                                    if (collection.address.length > 0) {
                                                        navigate('/collection/' + collection.address)
                                                    }
                                                }}
                                            >
                                                <DoubleArrowIcon disabled={!collection.valid}/>
                                            </IconButton>
                                        </Tooltip>
                                    }
                                </InputAdornment>
                            ),
                        }}
                        variant="outlined"
                    />
                </Stack>
            </Paper>
            <Paper
                elevation={4}
                sx={{
                    padding: '1rem'
                }}>
                <Government/>
            </Paper>
        </Stack>
    )
}